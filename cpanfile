requires 'Eixo::Base', '> 1.400';
requires 'LWP::UserAgent',  '>= 6.06';
requires 'LWP::Protocol::https', '>= 6.06';
requires 'DBI', '>= 1.630, < 2.0';
#requires 'DBD::SQLite', '>= 1.40, < 2.0';
requires 'DBD::mysql', '>= 4.033, < 5';

requires 'Email::MIME', '>=1.94';
requires 'Email::Sender::Simple', '>=1.3';
requires 'Email::Valid', '>=1.2';

requires 'WWW::Telegram::BotAPI', '>=0.10';

