package EixoUtils::Email;

use strict;
use Eixo::Base::Clase;

use Email::Valid;
use Email::MIME;
use Email::Sender::Simple qw(sendmail);
use Email::Sender::Transport::SMTP;

has(

    a => undef,
    desde=>undef,
    asunto=>undef,
    cuerpo=>undef,
    charset=>'UTF-8',
    __mensaje=>undef,
    servidor_correo=>undef,

);

sub crearEmail{
    my ($self, $atributos) = @_;

    $atributos = $self->__validarAtributosEmail($atributos);

    #print Dumper($atributos); use Data::Dumper;

    $self->__mensaje(

        Email::MIME->create(

            header_str => [

                From => $atributos->{desde},
                To => [(split /\,/, $atributos->{a})],
                Subject => $atributos->{asunto}
                

            ],

            attributes=> {

                encoding=>"quoted-printable",
                charset=>$atributos->{charset},

            },

            body_str => $atributos->{cuerpo}

        )

    );
    

    return $self;

}

sub enviar{
    my ($self) = @_;

    if(!$self->__mensaje){

        $self->__error(
            "No hay ningún email creado. No se puede enviar"
        )
    }

    my $conf = $self->__prepararServidorEnvio;

    if($conf){
        sendmail($self->__mensaje, {transport=>$conf})
    }
    else{
        sendmail($self->__mensaje);   
    }

}

sub __prepararServidorEnvio{
    my ($self) = @_;

    if($self->servidor_correo){

        return Email::Sender::Transport::SMTP->new(

            ssl=>1,
            host=>$self->servidor_correo->{host},
            sasl_username=>$self->servidor_correo->{username},
            sasl_password=>$self->servidor_correo->{password},
            port=>$self->servidor_correo->{port},
            #debug=>1
        )


    }
    else{
        return undef;
    }
}

sub __validarAtributosEmail{
    my ($self, $atributos) = @_;
 
    $atributos->{a} |= $self->a || $self->__error(
        "Falta la direccion del destinatario"
    );   

    foreach my $email (split/\,/, $atributos->{a}){
        unless($self->__emailValido($email)){
            $self->__error("Direccion de email no valida " . $email);
        } 
    }

    $atributos->{desde} |= $self->desde || $self->__error(
        "Falta la direccion del remitente"
    );

    unless($self->__emailValido($atributos->{desde})){
        $self->__error("Direccion del remitente no es valida " . $atributos->{desde});
    }

    $atributos->{asunto} |= $self->asunto || "";

    $atributos->{cuerpo} |= $self->cuerpo || "";
     

    $atributos->{charset} |=  $self->charset;
    
    return $atributos;
}


sub __emailValido :Sig(self, s){
    my ($self, $email) = @_;

    return Email::Valid->address($email)
}


sub __error{

    die(__PACKAGE__ . ": ", @_[1..$#_])
}


1;
