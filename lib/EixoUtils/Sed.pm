package EixoUtils::Sed;

use strict;
use Eixo::Base::Clase qw(EixoUtils::Base);
use EixoUtils::Runner;
use IPC::Cmd qw(can_run);

has (
    file     => undef,
    oldtext  => undef,
    newtext  => undef,
    sed_path => can_run('sed')
);

sub initialize{
    my ($self, %args) = @_;

    $self->SUPER::initialize(%args);

    $self->verifyRequirements();
}

sub verifyRequirements {
    $_[0]->__checkSed();
}

sub __checkSed {
    die('sed not found') unless( -x $_[0]->sed_path() );
}

sub replace {
    my ($self, %args) = @_;

    my %merged_args = (%$self, %args);

    $self->mustExistParameters(
        [qw(file oldtext newtext)],
        \%merged_args
    );

    $self->__execSed(
        args => [],
        options => \%merged_args
    );
}

sub __execSed {

    my ( $self, %params ) = @_;

    $self->__exec(
        cmd             => $self->sed_path(),
        arguments       => $params{args},
        options         => $params{options},
        general_options => [qw(file oldtext newtext)],
    );
}

sub __processGeneralOptions {
    my ( $self, %params ) = @_;

    my @args = ();

    my %options = %{$params{options} || {}};

    # sed -i 's/target/replace/' file

    push (
        @args, 
            '-i', 's/'.$options{oldtext}.'/'.$options{newtext}.'/',
            $options{file}
    );
    
    return @args;
}

1;
