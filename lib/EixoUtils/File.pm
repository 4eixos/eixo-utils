package EixoUtils::File;
use strict;
use LWP::UserAgent;


sub get_contents{
    my ($url) = @_;

    # de momento so soportamos :
    # http(s)
    # file

    if($url =~ m!^(https?|ftp)\://! ){

        my $ua = LWP::UserAgent->new;
        my $response = $ua->get($url);

        die $response->status_line if !$response->is_success;
        
        return $response->content;

    }
    elsif($url =~ m!^(file\://)?(.+)!){

        my $file = $2;
        open my $fh, $file || die("$file, $!");

        my $data;
        {
            local $/;
            $data = <$fh>;
        }

        return $data;
    }
}

1;
