package EixoUtils::Zip;
#
# based in https://gist.github.com/eqhmcow/5389877
#

use strict;
use IO::Uncompress::Unzip qw(unzip $UnzipError);
use File::Spec::Functions qw(splitpath);
use IO::File;
use File::Path qw(mkpath);
use EixoUtils::File;

sub unzip_from_url {
    my ($url, $destination) = @_;

    my $contents = EixoUtils::File::get_contents($url);

    open my $fh, '<', \$contents;
    binmode($fh);
    
    unzip($fh, $destination);
}


sub unzip {
    my ($file, $dest) = @_;

    die 'Need a file argument' unless defined $file;
    $dest = "." unless defined $dest;

    my $u = IO::Uncompress::Unzip->new($file)
        or die "Cannot open $file: $UnzipError";

    my $status;
    for ($status = 1; $status > 0; $status = $u->nextStream()) {
        my $header = $u->getHeaderInfo();

        my (undef, $path, $name) = splitpath($header->{Name});
        my $destdir = "$dest/$path";

        unless (-d $destdir) {
            mkpath($destdir) or die "Couldn't mkdir $destdir: $!";
        }

        if ($name =~ m!^/?$!) {
            last if $status < 0;
            next;
        }

        my $destfile = "$dest/$path/$name";
        my $buff;
        my $fh = IO::File->new($destfile, "w")
            or die "Couldn't write to $destfile: $!";
        while (($status = $u->read($buff)) > 0) {
            $fh->write($buff);
        }
        $fh->close();
        my $stored_time = $header->{'Time'};
        utime ($stored_time, $stored_time, $destfile)
            or die "Couldn't touch $destfile: $!";
    }

    1;

}

1;

