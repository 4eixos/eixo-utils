package EixoUtils::WpCli;
use strict;
use Eixo::Base::Clase qw(EixoUtils::Base);
use IPC::Cmd qw(can_run);
use vars qw($AUTOLOAD);


my %COMMANDS = (
    cache => [qw(add decr delete flush get incr replace set type)],

    core => [qw(check-update download install is-installed multisite-convert multisite-install update update-db verifiy-checksums version)],

    cron => [qw(event schedule test)],

    db => [qw(check cli create drop export import optimize prefix query repair reset search size tables)],

    export => [qw()],

    import => [qw()],

	package=> [qw(browse install list path uninstall update)],


    plugin => [qw(activate deactivate delete get install is-installed list path search status toggle uninstall update verify-checksums)],

    "search-replace" => [qw()],

    taxonomy => [qw(get list)],

    term => [qw(create delete generate get list meta recount update)],

    theme =>[qw(activate delete disable enable get install is-installed list mod path search status update)] ,
    user_cmd => [qw(add-cap add-role create delete generate get import-csv list list-caps meta remove-cap remove-role reset-password session set-role spam term unspam update)] ,

    widget => [qw(add deactivate delete list move reset update)],
);


my @COMMANDS_WITHOUT_SUBCOMMANDS = qw(export import search-replace);

my %COMMAND_OPTIONS = ();

sub SWITCH_OPTIONS{
    my $command = $_[0];

    my @global = qw(allow_root skip_packages skip_plugins skip_themes debug quiet) ;

    #locuron: haberia que cargar toda as opcions dun yaml/json
    #return [@global,$COMMAND_OPTIONS{$command}{switch_options}]

    return [@global];

}

sub GENERAL_OPTIONS{
    my $com = $_[0];

    my @global = qw(path url http user);

    #locuron: haberia que cargar toda as opcions dun yaml/json
    #return [@global,$COMMAND_OPTIONS{$command}{switch_options}]
    return [@global]
}


has(

    wp_path => can_run('wp'),
    path => undef,
    url => undef,
    http => undef,
    user => undef,
    allow_root => 1,
    skip_plugins => undef,
    skip_themes => undef,
    skip_packages => undef,
    debug => undef,
    quiet => undef,
    format => 'json',
    locale => 'es_ES',
);

sub initialize{
    my ($self, %args) = @_;

    $self->SUPER::initialize(%args);

    $self->verifyRequirements();
}

sub verifyRequirements{
    $_[0]->__checkWp();
}

sub __checkWp{
    die('wp not found')
        unless(-x $_[0]->wp_path);
}


#
# $self->db('subcommand', @args, $options={});
#
sub AUTOLOAD {

    my $command = (split(/\:\:/, $AUTOLOAD))[-1];
    my $real_command = "";

    if($command =~ /_cmd$/){
        ($real_command) = $command =~ /^(.+)_cmd$/;
    }
    else{

        $real_command = $command;
    }

    my ($self, @args) = @_;

    my $subcommand = undef;

    unless(grep {$_ eq $command} @COMMANDS_WITHOUT_SUBCOMMANDS){
        $subcommand = shift(@args);
    }



    # hash de opcions ao final dos parametros
    my $options = scalar(@args) && ref($args[-1]) eq 'HASH' ? pop(@args) : {};

    my %merged_options = (%$self, %$options);

    if(!$self->validCommand($command)){
        die("Invalid wp-cli command '$command'");
    }

    if($subcommand && !$self->validSubcommand($command, $subcommand)){
        die("Invalid subcommand in wp $command");
    }



    $self->__execWp(
        command => $real_command,
        subcommand => $subcommand,
        args => \@args,
        options => \%merged_options,
        switch_options => SWITCH_OPTIONS($command),
        general_options => GENERAL_OPTIONS($command),
    )

}

sub validCommand {
    my ($self, $com) = @_;

    scalar(grep {$com eq $_} keys(%COMMANDS))
}

sub validSubcommand {
    my ($self, $com, $subc) = @_;

    scalar(grep {$subc eq $_} @{$COMMANDS{$com}})

}



sub __execWp{
    my ($self,%params) = @_;

    my $arguments = [$params{command}];
    push @$arguments, $params{subcommand} if($params{subcommand});
    push @$arguments, @{$params{args}} if(@{$params{args}});

    $self->__exec(
        cmd => $self->wp_path,
        arguments => $arguments,
        options => $params{options},

        switch_options => $params{switch_options} || [],
        general_options => $params{general_options} || []
    );

}

sub __processGeneralOptions{
    my ($self, %params) = @_;

    my @args = $self->SUPER::__processGeneralOptions(%params);

    my @res;

    # aqui necesitamos qu vaian unidos por '='
    for ( my $i=0; $i<@args; $i=$i+2 ) {
        push @res, "$args[$i]=$args[$i+1]";
    }

    @res;
}


