package EixoUtils::Mongorestore;

use strict;
use Eixo::Base::Clase qw(EixoUtils::Base);
use EixoUtils::Runner;
use IPC::Cmd qw(can_run);

has(
    host => '127.0.0.1',
    port => 27017,
    username => undef,
    password => undef,
    db => undef,
    archive => undef,
    dir  => undef,
    gzip => undef,
    drop => undef,

    mongorestore_path => can_run('mongorestore'),

);

sub initialize{
    my ($self, %args) = @_;

    $self->SUPER::initialize(%args);
    
    $self->verifyRequirements();

}

sub verifyRequirements{
    $_[0]->__checkMongorestore();
}

sub __checkMongorestore{
    die('mongorestore not found') 
        unless(-x $_[0]->mongorestore_path);
}

sub restore{
    my ($self, %args) = @_;

    my %merged_args = (%$self, %args);

    $self->mustExistParameters(
        [qw(
            archive|dir
            host 
            db 
        )],
        \%merged_args
    );

    $self->__execMongorestore(
        args => [],
        options => \%merged_args 
    );

}


sub __execMongorestore{

    my ($self,%params) = @_;

    $self->__exec(
        cmd => $self->mongorestore_path,
        arguments => $params{args},
        options => $params{options},


        switch_options => [qw(
            gzip
            drop
        )],

        general_options => [qw(
            host
            port
            username
            password
            db
            archive
            dir
        )],
    );
}

sub __processGeneralOptions{
    my ($self, %params) = @_;

    my @args = ();

    my %options = %{$params{options} || {}};

    foreach my $opt (@{$params{general_options}}){
        if(my $val = $options{$opt}){
            my $cmd_opt = $opt;
            push @args,"--$cmd_opt=$val"; 
        }
    }

    return @args;
}

1;
