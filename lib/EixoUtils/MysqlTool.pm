package EixoUtils::MysqlTool;

use strict;
use EixoUtils::Runner;
use DBI;
use DBD::mysql;

my $EJECUTABLE_MYSQL = `which mysql` || die("Can't find mysql client");
my $EJECUTABLE_MYSQLDUMP = `which mysqldump` || die("Can't find mysqldump");
chomp($EJECUTABLE_MYSQLDUMP, $EJECUTABLE_MYSQL);

sub new {
    my ($class, %datos) = @_;
    die("Es necesario remitir los datos de la bbdd") unless(%datos);

    my $hostname = `/bin/hostname`;chomp($hostname);

    my $self = bless({

        usuario => $datos{usuario},
        clave => $datos{clave},
        bd => $datos{bd},
        host => $datos{host} || $hostname,
        puerto => $datos{puerto} || 3306,
        reparada => undef,
        exportada => undef
    });
    
    $self->obtenerVersion;
    
    return $self;

}


sub conectaDB {
    my ($self) = @_;

    my $conn;
    # nos conectamos a la bbdd
    eval{
	
        $conn = DBI->connect(
            'dbi:mysql:'.$self->{bd}.':'.$self->{host}.':'.$self->{puerto},
            $self->{usuario}, $self->{clave}, 
            {RaiseError=>1}
        );	
    };
    if($@){
        die("Connection error: $@");
    }

    return $conn;

}

sub exportarBD{
    my $self = $_[0];
    my $fichero_export = $_[1] || die("Falta el fichero a donde exportar la bd");
    
    $self->{fichero_exportacion} = $fichero_export;
    
    if($self->{version_mysql} =~ /^5\./){
    	$self->exportarBDMysql5;
    }
    elsif($self->{version_mysql} =~ /^4\./){
    	$self->exportarBDMysql4;
    }	
    else{
    	die(ref($self).'->_producirDumpBaseDatos: version de mysql no soportada: '.$self->{version_mysql});
    }
    
    $self->finalizarExportacion;

}


#
sub exportarTablas {
    my $self = $_[0];
    my $ruta_exportacion = $_[1] || die("Falta la ruta donde exportar la tabla de la base de datos");
    my $tablas = $_[2] || die("Falta las tablas para la exportacion");
    
    $self->{fichero_exportacion} = $ruta_exportacion;
    
    if($self->{version_mysql} =~ /^5\./){
        $self->exportarTablasMysql5(@$tablas);
    }
    elsif($self->{version_mysql} =~ /^4\./){
        $self->exportarTablasMysql4(@$tablas);
    }
    else{
        die(ref($self).'->_producirDumpBaseDatos: version de mysql no soportada: '.$self->{version_mysql});
    }
    
    $self->finalizarExportacion();
}

#      metodo: exportadorPorPartes
#
# descripcion: Exporta, por separado, cada tabla de la base de datos con sus
#              triggers asociados, creando un fichero SQL para cada una y
#              otro con los procedimientos almacenados.
#
#  parametros: Ruta a una carpeta donde crear el fichero
#
sub exportadorPorPartes {
    my $self = $_[0];
    my $ruta_exportacion = $_[1] || 
        die("Falta la ruta donde exportar la tabla de la base de datos");
    
    eval {
        # Obtenemos la lista de tablas 
        my $tablas_disponibles = $self->getTablasExtendido();
        
        # Exportamos las tablas
        foreach my $tabla (sort {$a cmp $b} keys(%$tablas_disponibles)) {
            $self->exportarTabla("$ruta_exportacion/$tabla.sql", $tabla);
        }
        
        # Exportamos los procedimientos almacenados de la tabla
        $self->exportarProcedimientos("$ruta_exportacion/stored_procedures.sql");
    };
    
    if($@) {
        die("Error exportando la base de datos: $@");
    }
    
    return 1;
}



sub exportarProcedimientos {

    return 1 if($_[0]->{version_mysql} =~ /^4\./); 
    # MySQL 4 no soporta procedimientos almacenados, no exportamos nada

    $_[0]->__exportar(
        
        options => [
            '--routines',
            '--no-create-info',
            '--no-data',
            '--no-create-db',
            '--create-options',
            '--skip-triggers'
        ]
    );

}


sub exportarBDMysql4 {

    $_[0]->__exportar(

        options => [qw(
            --opt
            -f
            -Q
            --allow-keywords
        )],
    
    );

}

sub exportarBDMysql5{

    $_[0]->__exportar(
        options => [qw(
            --opt
            -f
            -Q
            --allow-keywords
            --routines
            --triggers
        )],
    )

}


sub exportarTablasMysql4{
    my ($self, @tablas) = @_;

    $self->__exportar(

        options => [qw(
            --opt
            -f
            -Q
            --allow-keywords
        )],

        tables => [@tablas]
    )

}


sub exportarTablasMysql5{
    my ($self, @tablas) = @_;

    $self->__exportar(

        options => [qw(
            --opt
            -f
            -Q
            --allow-keywords
        )],
        tables => [@tablas]
    )
}



sub finalizarExportacion {
    my $self = $_[0];
    my $file = $_[1] || $self->{fichero_exportacion};
    
    open (FILE ,$file);
    open FILE2, '>', $file.".new";
    
    print FILE2 'SET AUTOCOMMIT=0;'."\n";
    print FILE2 'SET FOREIGN_KEY_CHECKS=0;'."\n";
    
    while (my $linea = <FILE>){
            # Parche para as vistas
            # $linea =~ s/DEFINER=\`[^\`]+\`@\`%\`/DEFINER=CURRENT_USER/g;
            $linea =~ s/DEFINER=\`[^\`]+\`@\`[^\`]+\`/DEFINER=CURRENT_USER/g;
            $linea =~ s/TYPE\=/ENGINE\=/g;
            $linea =~ s/timestamp\(\d+\)/timestamp/ig;
            print FILE2 $linea;
    }
    print FILE2 'SET AUTOCOMMIT=1;'."\n";
    print FILE2 'SET FOREIGN_KEY_CHECKS=1;'."\n";
    close FILE2;
    
    close FILE;
    
    EixoUtils::Runner->new->run('/bin/mv', '-f', $file.".new", $file);

}



sub importarBD{
    my ($self, $file) = @_;

    EixoUtils::Runner->new(die_on_error => 1)->run(

        $EJECUTABLE_MYSQL,
        '-u',$self->{usuario},
        '-p'.$self->{clave},
        '-h', $self->{host},
        '-P', $self->{puerto},
        '-e',"source $file",
        $self->{bd}
    );

}

sub ejecutarSql{
    my ($self, $sentencia) = @_;

    EixoUtils::Runner->new(die_on_error => 1)->run(
        $EJECUTABLE_MYSQL,
        '-u',$self->{usuario},
        '-p'.$self->{clave},
        '-h', $self->{host},
        '-P', $self->{puerto},
        '-e',$sentencia
    );
}


sub repararTablasBD{
    my $self = $_[0];
    
    my @lista_tablas = $self->getReparableTables;
    
    if(@lista_tablas > 0){
        
        $self->conectaDB()->do(
            
            'REPAIR TABLES '.

            join(',', map {"`$_`"} @lista_tablas)
        ); 
    }
    
    $self->{reparada} = 1;
}



sub obtenerVersion{
    my $self = $_[0];
    
    my $dbh = $self->conectaDB();

    $self->{version_mysql} = $dbh->get_info(18);

}


##
#
#      metodo: getTablasExtendido
#
# descripcion: Obtiene las tablas y vistas de las que se compone una base de datos
#              con sus datos asociados (en el caso de las vistas, todos los campos
#              del hash seran NULL/undef, salvo el nombre y el comentario que son,
#              respectivamente, el nombre de la vista y 'view'
#
sub getTablasExtendido {
    my $self = $_[0];
    
    my $conexion = undef;
    my $tablas = [];
    
    my $sth = $self->conectaDB()->prepare('SHOW TABLE STATUS;');

    $sth->execute();
    
    return $sth->fetchall_hashref('Name');
}



sub getReparableTables {
    my ($self) = @_;

    map {
        $_->{Name}
        
    } grep {
        
        !$_->{Engine} || $_->{Engine} =~ /MyISAM|ARCHIVE|CSV/

    } values(%{$self->getTablasExtendido});

}




sub __exportar{
    
    my ($self, %args) = @_;

    # pillamos o metodo que chamou a esta funcion
    my $metodo_caller = (caller(1))[3];

    eval {
        
        EixoUtils::Runner->new(die_on_error => 1)->run(

            $EJECUTABLE_MYSQLDUMP,

            '-h',$self->{host},
            '-u',$self->{usuario},
            '-p'.$self->{clave},
            
            # opcions extra segun o tipo de exportacoin
            @{$args{options}},

            # bbdd
            $self->{bd},

            # lista opcional de tablas
            @{ $args{tables} || [] },

            # fichero de salida
            '--result-file='.$self->{fichero_exportacion}
            #">".$self->{fichero_exportacion}));
        );
    };
    if($@){
	if($@ =~ /insufficent privileges|access denied/i){

            die($@) if($self->{exportada});

            $self->{exportada} = 1;
	    $self->{host} = 'localhost';
	    $self->$metodo_caller();
 	}
	elsif(!$self->{reparada}){
	    eval {
	        $self->repararTablasBD;
	    };
	    if ($@) {
	        die('Error en exportacion de bd, no se pueden reparar las tablas: ' .$@);
	    }

	    $self->$metodo_caller;
	}
	else{
	    die('Error en exportaciond de bbdd : '.$@) ;
	}
    }

}


1;
