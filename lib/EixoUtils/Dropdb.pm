package EixoUtils::Dropdb;

use strict;
use Eixo::Base::Clase qw(EixoUtils::Base);
use EixoUtils::Runner;
use IPC::Cmd qw(can_run);


has(
    host          => '127.0.0.1',
    port          => 5432,
    username      => undef,
    password      => undef,
    db            => undef,
    dropdb_path   => can_run('dropdb'),
);

sub initialize{
    my ($self, %args) = @_;

    $self->SUPER::initialize(%args);
    
    $self->verifyRequirements();
}

sub verifyRequirements{
    $_[0]->__checkDropdb();
}

sub __checkDropdb {
    die('psql not found') 
        unless( -x $_[0]->dropdb_path() );
}

sub drop {
    my ($self, %args) = @_;
    my %merged_args = (%$self, %args);

    $self->mustExistParameters(
        [qw(host db username password)],
        \%merged_args
    );

    $self->__execDropdb(
        args => [],
        options => \%merged_args,
    );
}

sub __execDropdb {
    my ($self, %params) = @_;

    $self->__exec(
        cmd       => $self->dropdb_path,
        arguments => $params{args},
        options   => $params{options},

        general_options => [qw(
            host
            port
            username
            password
            db
        )],
    );
}

sub __processGeneralOptions{
    my ($self, %params) = @_;

    my @args = ();

    my %options = %{$params{options} || {}};

    push @args, $options{db};

    return @args;
}

sub prepareEnv{
    my($self, %params) = @_;

    my $env = {
        PGUSER     => $params{options}->{username},
        PGPASSWORD => $params{options}->{password},
        PGHOST     => $params{options}->{host},
    };

    return $env;
}

1;
