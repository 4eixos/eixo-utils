package EixoUtils::Pgdump;

use strict;
use Eixo::Base::Clase qw(EixoUtils::Base);
use EixoUtils::Runner;
use IPC::Cmd qw(can_run);


has(
    host         => '127.0.0.1',
    port         => 5432,
    username     => undef,
    password     => undef,
    db           => undef,
    file         => undef,
    owner        => undef,
    clean        => undef,
    create       => undef,
    pg_dump_path => can_run('pg_dump'),
);


sub initialize{
    my ($self, %args) = @_;

    $self->SUPER::initialize(%args);
    
    $self->verifyRequirements();

}

sub verifyRequirements{
    $_[0]->__checkPgdump();
}

sub __checkPgdump{
    die('pg_dump not found') 
        unless(-x $_[0]->pg_dump_path);
}

sub dump{
    my ($self, %args) = @_;

    my %merged_args = (%$self, %args);

    $self->mustExistParameters(
        [qw(host db username password file)],
        \%merged_args
    );

    $self->__execPgdump(
        args => [],
        options => \%merged_args 
    );

}


sub __execPgdump{

    my ($self,%params) = @_;

    $self->__exec(
        cmd => $self->pg_dump_path,
        arguments => $params{args},
        options => $params{options},


        general_options => [qw(
            host
            port
            username
            password
            db
            file
            owner
        )],
    );
}

sub __processGeneralOptions{
    my ($self, %params) = @_;

    my @args = ();

    my %options = %{$params{options} || {}};

    # Creamos la uri de la base de datos
    my $dburi = 'postgresql://'.$options{host} . ':' . $options{port} . '/' . $options{db};

    push(@args, '-U', $options{username});

    push(@args, "--dbname=$dburi", "--file=$options{file}");

    push(@args, "--no-owner") unless( $options{owner} );

    push(@args, "--create") if ( $options{create} );

    push(@args, "--clean") if ( $options{clean} );

    return @args;
}


sub prepareEnv{
    my($self, %params) = @_;

    my $env = {
        PGPASSWORD => $params{options}->{password}
    };

    return $env;
    
}

1;

