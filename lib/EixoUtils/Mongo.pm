package EixoUtils::Mongo;

use strict;
use Eixo::Base::Clase qw(EixoUtils::Base);
use EixoUtils::Runner;
use IPC::Cmd qw(can_run);

has(
    host => '127.0.0.1',
    port => 27017,
    username => undef,
    password => undef,
    db => undef,
    script => undef,

    mongo_path => can_run('mongo'),

);

sub initialize{
    my ($self, %args) = @_;

    $self->SUPER::initialize(%args);
    
    $self->verifyRequirements();

}

sub verifyRequirements{
    $_[0]->__checkMongo();
}

sub __checkMongo{
    die('mongo not found') 
        unless(-x $_[0]->mongo_path);
}

sub restore{
    my ($self, %args) = @_;

    my %merged_args = (%$self, %args);

    $self->mustExistParameters(
        [qw(
            script
            host 
            db 
        )],
        \%merged_args
    );

    $self->__execMongo(
        args => [],
        options => \%merged_args 
    );

}


sub __execMongo{

    my ($self,%params) = @_;

    $self->__exec(
        cmd => $self->mongo_path,
        arguments => $params{args},
        options => $params{options},

        general_options => [qw(
            host
            port
            username
            password
            db
            script
        )],
    );
}

sub __processGeneralOptions{
    my ($self, %params) = @_;

    my @args = ();

    my %options = %{$params{options} || {}};

    foreach my $opt (@{$params{general_options}}){
        next if( $opt =~ /^(script|db)$/ );
        if(my $val = $options{$opt}){
            my $cmd_opt = $opt;
            push @args,"--$cmd_opt=$val"; 
        }
    }

    push @args, $options{db}, $options{script};

    return @args;
}

1;
