package EixoUtils::Duplicity;
use strict;
use Eixo::Base::Clase qw(EixoUtils::Base);
use EixoUtils::Runner;
use IPC::Cmd qw(can_run);

#### properties
my $DEFAULT_ARCHIVE_DIR = '/var/cache/duplicity';
my $DEFAULT_BACKEND = 'file';


has(
    encrypt_key =>undef,
    sign_key => undef,
    encrypt_secret_keyring => undef,
    backup_type => undef,
    action => undef,
    archive_dir => $DEFAULT_ARCHIVE_DIR,
    source_path => '/',
    target_url => undef,
    source_url => undef,
    target_folder=> undef,
    volsize => undef,
    full_if_older_than => undef,

    backend_config => {

        backend => $DEFAULT_BACKEND,
        user => undef,
        secret => undef,
        container => undef,
    },

    s3_use_new_style => 1,

    passphrase => undef,
    sign_passphrase => undef,
    tmpdir => undef,

    force => 1 , #default to remove files intead of list
    log_fd => 1, #default to stdout,
    log_file => undef, #default to stdout,
    include => undef,
    exclude => undef,
    no_encryption => undef,
    no_compression => undef,
    allow_source_mismatch => undef,
    duplicity_path => can_run('duplicity'),
    gpg_path => can_run('gpg'),
);


sub build_backend_url{
    my ($self, $folder) = @_;

    $folder ||= '';
    
    my $bconfig = $_[0]->backend_config;

    my $url;

    if($bconfig->{backend} eq 'azure'){

        $url = 'azure://'. ($folder || $bconfig->{container})

    }
    elsif($bconfig->{backend} eq 's3'){
        
        $url = $bconfig->{backend}.'+http://'.$bconfig->{container};
        $url .= '/'.$folder if($folder);
    }
    elsif($bconfig->{backend} eq 'file'){
        $url = $bconfig->{backend}.'://'.$bconfig->{container};
        $url .= '/'.$folder if($folder);
    
    }
    #elsif($bconfig->{backend} eq 'rsync') {
    #    
    #}
    else{
        die("Unsupported Backend: '".$bconfig->{backend}."'");
    }

    return $url;

}



### verifications
# chequear que duplicity esta instalado
# se lle pasan keys 
# - chequear que gpg este instalado
# - chequear que a key exista no gpg keyring
# - chequear que este seteada no entorno a passphrase
# - que se consiga descifrar a key correctamente
# ao facer backup que exista o source

sub verifyRequirements{

    $_[0]->__checkDuplicity();
    $_[0]->__checkGpg();
    $_[0]->__checkGpgKeys();
}

sub __checkDuplicity{
    die('duplicity not found') 
        unless($_[0]->duplicity_path);

}

sub __checkGpg{

    die("GPG not found")
        unless($_[0]->gpg_path);
}

sub __checkGpgKeys{
    ## testear que as keys estan dadas de alta no gpg
}


#### methods

# backup
#
sub fullBackup{
    my ($self, %args) = @_;

    $self->backup(%args, backup_type => 'full')
}

sub incrementalBackup{
    my ($self, %args) = @_;

    $self->backup(%args, backup_type => 'incr')
}

sub backup{
    my ($self,%args) = @_;

    # merge args with object properties
    my %merged_args = (%$self, %args);

    ## validations
    $self->mustExistParameters(
        [qw(source_path target_url|target_folder)], 
        \%merged_args
    );

    die("Source path '$merged_args{source_path}' not exists in filesystem") unless(-e $merged_args{source_path});

    # 
    # si so nos pasan o target_folder, construimos a url cos parametros do backend
    #
    $merged_args{target_url} = $merged_args{target_url} ||
        $self->build_backend_url($merged_args{target_folder});

    my @args;

    if($merged_args{backup_type}){
        push @args, $merged_args{backup_type}
    }

    foreach my $inc (@{$merged_args{include}}){
        push @args, '--include', $inc;
    };

    foreach my $exc (@{$merged_args{exclude}}){
        push @args, '--exclude', $exc;
    };

    # 
    # other duplicity options
    #
    foreach my $opt (qw(
        exclude_filelist 
        exclude_if_present
        exclude_older_than
        exclude_regexp
        volsize
        full_if_older_than
    )){
        if(my $val = $merged_args{$opt}){
            my $duplicity_opt = ($opt =~ tr/_/-/r);
            push @args,"--$duplicity_opt", $val; 
        }
    }

    #
    # other duplicity flag options
    #
    foreach my $opt (qw(
        allow_source_mismatch
        copy_links
        exclude_device_files
        exclude_other_filesystems
        
    )){

        if(my $val = $merged_args{$opt}){
            my $duplicity_opt = ($opt =~ tr/_/-/r);
            push @args,"--$duplicity_opt"; 
        }
    }

    # source and destination
    push @args, $merged_args{source_path}, $merged_args{target_url};

    $self->__execDuplicity(\@args, \%merged_args);
}


#
# verify compares the latest backup with the current files
#
sub verify{
    my ($self, %args) = @_;

    my %merged_args = (%$self, %args);

    ## validations
    $self->mustExistParameters(
        [qw(source_url|source_folder target_folder)], 
        \%merged_args
    );

    $merged_args{source_url} = $merged_args{source_url} || 
        $self->build_backend_url($merged_args{source_folder});

    my @cmd = (
        "verify",
        $merged_args{source_url},
        $merged_args{target_folder}
    );

    if($merged_args{'compare_data'}){
        push @cmd, '--compare-data';
    }

    if(my $time = $merged_args{time}){
        push @cmd, '--time', $time;
    }

    $self->__execDuplicity(\@cmd,\%merged_args)

}


# 
# restore a backup
#
sub restore{
    my ($self, %args) = @_;

    my %merged_args = (%$self, %args);

    ## validations
    $self->mustExistParameters(
        [qw(source_url|source_folder restoration_path)], 
        \%merged_args
    );

    $merged_args{source_url} = $merged_args{source_url} ||
        $self->build_backend_url($merged_args{source_folder});

    my @cmd = (
        "restore",
        $merged_args{source_url},
        $merged_args{restoration_path}
    );

    if(my $file_to_restore = $merged_args{file_to_restore} ){
        push @cmd, '--file-to-restore', $file_to_restore;
    }

    if(my $time = $merged_args{time}){
        push @cmd, '--time', $time;
    }

    $self->__execDuplicity(
        \@cmd,
        \%merged_args
    );
}

#
# check collection status
#
sub collectionStatus{
    my ($self, %args) = @_;
    my %merged_args = (%$self, %args);

    ## validations
    $self->mustExistParameters(
        [qw(source_url|source_folder)], 
        \%merged_args
    );

    $merged_args{source_url} = $merged_args{source_url} ||
        $self->build_backend_url($merged_args{source_folder});

    my @cmd = (
        "collection-status",
        $merged_args{source_url}
    );

    if(my $file_changed = $merged_args{file_changed}){
        push @cmd, '--file-changed', $file_changed;
    }

    $self->__execDuplicity(\@cmd,\%merged_args);
}

# 
# list files in a backup
#
sub listCurrentFiles{
    my ($self, %args) = @_;
    my %merged_args = (%$self, %args);

    ## validations
    $self->mustExistParameters(
        [qw(source_url|source_folder)], 
        \%merged_args
    );

    $merged_args{source_url} = $merged_args{source_url} ||
        $self->build_backend_url($merged_args{source_folder});


    my @cmd = (
        "list-current-files",
        $merged_args{source_url}
    );
    

    if(my $time = $merged_args{time}){
        push @cmd, '--time', $time;
    }
    
    $self->__execDuplicity(
        \@cmd,
        \%merged_args
    );
}


#
# cleanup archive_dir 
#
sub cleanup{
    
    my ($self,%args) = @_;

    my %merged_args = (%$self, %args);
    
    $self->mustExistParameters(
        [qw(target_url|target_folder)], 
        \%merged_args
    );

    $merged_args{target_url} = $merged_args{target_url} ||
        $self->build_backend_url($merged_args{target_folder});

    my @cmd = (
        'cleanup',
        $merged_args{'target_url'}
    );

    if($merged_args{'force'}){
        push @cmd, '--force';
    }

    $self->__execDuplicity(
        \@cmd,
        \%merged_args
    );
}

# 
# purgue older backups
#
sub removeOlderThan{
    my ($self, %args) = @_;

    my %merged_args = (%$self, %args);
    
    
    ## validations
    $self->mustExistParameters(
        [qw(target_url|target_folder time)], 
        \%merged_args
    );

    $merged_args{target_url} = $merged_args{target_url} ||
        $self->build_backend_url($merged_args{target_folder});

    my @cmd = (
        'remove-older-than',
        $merged_args{time},
        $merged_args{target_url},
    );

    if($merged_args{'force'}){
        push @cmd, '--force';
    }

    $self->__execDuplicity(
        \@cmd,
        \%merged_args
    );
}

sub __execDuplicity{
    my ($self, $args, $options) = @_;

    # extract action in execution
    $self->{action} = $args->[0];

    $self->__exec(
        cmd => $self->duplicity_path,
        arguments => $args,
        options => $options,

        switch_options => [qw(

            dry_run
            no_compression
            no_encryption
            s3_use_new_style
        
        )],

        general_options => [qw(
            
            volsize
            archive_dir
            encrypt_key
            sign_key
            log_fd
            log_file
            time
        
        )],
    );

    return $self;
}


#
# environment vars
#
#FTP_PASSWORD
#PASSPHRASE
#SIGN_PASSPHRASE
#
#Azure Backend
#AZURE_ACCOUNT_NAME
#AZURE_ACCOUNT_KEY
#
#S3 Backend
#------------
#AWS_ACCESS_KEY_ID
#AWS_SECRET_ACCESS_KEY

sub prepareEnv{
    my ($self) = @_;

    my $bconfig = $self->backend_config;
    my %backend_credentials = ();
    
    if($bconfig->{backend} eq 'azure'){

        %backend_credentials = (
            AZURE_ACCOUNT_KEY => $bconfig->{key},
            AZURE_ACCOUNT_NAME => $bconfig->{user}
        )
    
    }
    elsif($bconfig->{backend} eq 's3'){
        %backend_credentials = (
            AWS_SECRET_ACCESS_KEY => $bconfig->{key} ,
            AWS_ACCESS_KEY_ID => $bconfig->{user}
        )
    }
    elsif($bconfig->{backend} eq 'ftp'){
        %backend_credentials = (
            FTP_PASSWORD => $bconfig->{key}
        )
    }

    # 
    # construimos o hash do env
    #
    my $env = {%backend_credentials};

    $env->{PASSPHRASE} = $self->passphrase 
        if($self->passphrase);

    $env->{SIGN_PASSPHRASE} = $self->sign_passphrase
        if($self->sign_passphrase);

    $env->{TMPDIR} = $self->tmpdir
        if($self->tmpdir);

    return $env;

}


1;

#duplicity cleanup --force --no-print-statistics --encrypt-key $encryptkey --sign-key $encryptkey --full-if-older-than $increments --archive-dir /var/cache/backupninja/duplicity $desturl
#
#duplicity remove-older-than $keep --force  --no-print-statistics --encrypt-key $encryptkey --sign-key $encryptkey --full-if-older-than $increments --extra-clean --archive-dir /var/cache/backupninja/duplicity $desturl
#
#duplicity --no-print-statistics --encrypt-key $encryptkey --sign-key $encryptkey --full-if-older-than $increments --extra-clean --archive-dir /var/cache/backupninja/duplicity  "
#for i in "${include[@]}"
#do
#        instr="$instr --include '$i'"
#done
#instr="$instr --exclude '**' / $desturl"
#
### Lista los ficheiros disponibles: 
#export PASSPHRASE=xxxxx
#duplicity list-current-files rsync://backups@backups.situm.es//media/backup-data/data/pre-old/sql --encrypt-key 41CBC7A8
#
### restaurar ficheiro dun backup
#duplicity -t 12D --file-to-restore var/lib/pgsql/backup-new/alarms.sql.gz  rsync://backups@backups.situm.es//media/backup-data/data/pro-old/sql --encrypt-key 41CBC7A8 /tmp/alarms.sql.gz
#
