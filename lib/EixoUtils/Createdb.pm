package EixoUtils::Createdb;

use strict;
use Eixo::Base::Clase qw(EixoUtils::Base);
use EixoUtils::Runner;
use IPC::Cmd qw(can_run);


has(
    host          => '127.0.0.1',
    port          => 5432,
    username      => undef,
    password      => undef,
    db            => undef,
    createdb_path => can_run('createdb'),
);

sub initialize{
    my ($self, %args) = @_;

    $self->SUPER::initialize(%args);
    
    $self->verifyRequirements();
}

sub verifyRequirements{
    $_[0]->__checkCreatedb();
}

sub __checkCreatedb {
    die('psql not found') 
        unless(-x $_[0]->createdb_path);
}

sub create {
    my ($self, %args) = @_;
    my %merged_args = (%$self, %args);

    $self->mustExistParameters(
        [qw(host db username password)],
        \%merged_args
    );

    $self->__execCreatedb(
        args => [],
        options => \%merged_args,
    );
}

sub __execCreatedb {
    my ($self, %params) = @_;

    $self->__exec(
        cmd       => $self->createdb_path,
        arguments => $params{args},
        options   => $params{options},

        general_options => [qw(
            host
            port
            username
            password
            db
        )],
    );
}

sub __processGeneralOptions{
    my ($self, %params) = @_;

    my @args = ();

    my %options = %{$params{options} || {}};

    push @args, $options{db};

    return @args;
}

sub prepareEnv{
    my($self, %params) = @_;

    my $env = {
        PGUSER     => $params{options}->{username},
        PGPASSWORD => $params{options}->{password},
        PGHOST     => $params{options}->{host},
    };

    return $env;
}

1;
