package EixoUtils::Base;
use strict;
use Eixo::Base::Clase;
use EixoUtils::Runner;
use Carp;

my $DEBUG = $ENV{EIXO_DEBUG};

sub debugOn{$DEBUG=1};
sub debugOff{$DEBUG=undef};



has(
    command_executed => undef,
    status => undef,
    output => undef,
    error => undef,
    die_on_error => 0,
    dry_run => undef,
    timeout => 0,
    env => undef,

);

sub initialize{
    my ($self, %args) = @_;

    $self->SUPER::initialize(%args);
    
    $self->verifyRequirements();

}

# to override
sub verifyRequirements{1}


# 
# chequea si existen os parametros especificados, e estan definidos
# dentro do target
#
sub mustExistParameters{
    
    my ($self, $parameters, $target) = @_;
    
    # if no target, search for parameter in $self
    $target ||= $self;

    foreach my $p (@$parameters){

        my @opcionales = split(/\|/, $p);

        unless(grep {exists($target->{$_}) && defined($target->{$_})} @opcionales){
            croak("Required parameter '$p' not specified")
        }

    }
}

# 
# clean target hash to remove parameters that should not be there 
#
sub shoudNotExistParameters{
    my ($self, $parameters, $target) = @_;
    
    # if no target, search for parameter in $self
    $target ||= $self;

    foreach my $p (@$parameters){
        delete($target->{$p});
    }

}


sub __exec{
    my ($self, %params) = @_;

    my $cmd = $params{cmd} ||
        croak("'cmd' is not provided");

    my @args = @{$params{arguments} || []};


    # 
    # general options
    #
    push @args, $self->__processGeneralOptions(%params);

    # 
    # switch  options
    #
    push @args, $self->__processSwitchOptions(%params);
    
    # 
    # environment
    #
    $self->env($self->prepareEnv(%params));

    my $on_change;

    if($DEBUG){
        print "running: ", join(' ', $cmd,@args), "\n";

        $on_change = sub {print $_[0]}
    }


    my $r = EixoUtils::Runner->new(
        die_on_error => $self->die_on_error,
        env => $self->env,
        en_mockup => $self->dry_run,
        timeout => $self->timeout,
        on_change => $on_change,
    )->run(
        $cmd, @args
    );

    $self->command_executed($r->command);

    return $self->processOutput($r);

}

sub __processGeneralOptions{
    my ($self, %params) = @_;

    my @args = ();

    my %options = %{$params{options} || {}};

    foreach my $opt (@{$params{general_options}}){
        if(my $val = $options{$opt}){
            my $cmd_opt = ($opt =~ tr/_/-/r);
            push @args,"--$cmd_opt", $val; 
        }
    }

    return @args;
}


sub __processSwitchOptions{
    my ($self, %params) = @_;

    my @args = ();

    my %options = %{$params{options} || {}};

    foreach my $opt (@{$params{switch_options}}){

        if(my $val = $options{$opt}){
            my $cmd_opt = ($opt =~ tr/_/-/r);
            push @args,"--$cmd_opt"; 
        }
    }

    return @args;
}


#
# to override
#
sub prepareEnv{
    return {}
}

sub processOutput{
    return $_[0]->__processOutputDefault($_[1]);
}



sub __processOutputDefault{
    my ($self, $runner) = @_;

    $self->{__runner} = $runner;

    if($DEBUG){
        print 'status:', $runner->status,"\n", "salida:", $runner->stdout,"\n", 'stderr:',$runner->stderr, "\n";
    }

    $self->status($runner->status);
    $self->output($runner->stdout.$runner->stderr);

    if($runner->status != 0){
        $self->error("Error in command (".join(' ', @{$runner->command}[0..1]). "). Details: ".$self->output);
    }

    return $self;
}

1;
