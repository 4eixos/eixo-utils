package EixoUtils::Telegram;

use strict;
use Eixo::Base::Clase;

use WWW::Telegram::BotAPI;

has(
    
    token=>undef,

    __api=>undef,
);

sub iniciar{
    my ($self) = @_;

    $self->__api(

        WWW::Telegram::BotAPI->new(
            token=>$self->token
        )
    );

    return $self;
    

}

sub info{

    $_[0]->api->getMe();
}

sub enviarMensaje :Sig(self, d, s){
    my ($self, $chat, $mensaje) = @_;

    $self->api->sendMessage({

        chat_id=>$chat,
        text=>$mensaje

    });
    
}

sub api{
    my ($self) = @_;

    if(!$self->__api){
        die(__PACKAGE__ ."::api no esta inicializada");
    }
 
    $self->__api;
}

sub getUpdates{
    my ($self) = @_;

    $self->api->api_request("getUpdates");
}

1;
