package EixoUtils::ArchivoTar;
use strict;
use Eixo::Base::Clase;
use EixoUtils::Runner;

####################
#     VARIABLES
#      GLOBALES
####################

my $RUTA_BINARIO_TAR = "/bin/tar";
my $RUTA_BINARIO_NICE = '/usr/bin/nice';
my $RUTA_BINARIO_IONICE = '/usr/bin/ionice';

#no requiere parametro alguno
#inicia la clase y las variables
#fundamentales. 
has(

    lineaTar => [],
    rutaArchivo => undef,
    archivoCreado => 0,
    prioridad => 19,
    permisosArchivo => 0600,
    puntoDesentarrado => undef,
    tar_extra_options => undef,
    siempre_completo => undef,
    forzar_creacion_archivo => 1,
    rutaSnap => undef,
    comprimir => 1,
    en_mockup => undef,
);

sub agregarRuta{

    my ($self, $ruta) = @_;

    $self->agregarFicheros($ruta);

    $self;

}


sub agregarFicheros{

    my ($self, @listado) = @_;

    return unless(@listado);

    push @{$self->{lineaTar}}, @listado;

    $self;
}

sub agregarRutasDesdeFichero{

    my ($self, $fichero) = @_;

    die("Fichero $fichero no existe") unless(-f $fichero);

    push @{$self->{lineaTar}}, "--files-from=$fichero";

    $self;

}


sub establecerRutaArchivo{

    my $self = $_[0];
    my $ruta = $_[1] || die("Error. ArchivoTar->establecerRutaArchivo : falta la ruta a establecer");

    $self->{rutaArchivo} = $ruta;

    $self;
}


sub crearArchivoTar{

    my $self = $_[0];

    if(!defined($self->{lineaTar})){

        die("Error. ArchivoTar->crearArchivoTar : no hay objetivos sobre los que crear el archivo tar");

    }

    if(!defined($self->{rutaArchivo})){

        die("Error. ArchivoTar->crearArchivoTar : falta la ruta del archivo tar a crear ");
    }

    if(!-e $RUTA_BINARIO_TAR){

        die("Error. ArchivoTar->crearArchivoTar: no se encuentra el binario del programa tar");

    }

    my $ret;

    my $opcion_comprimir = ($self->{comprimir})? 'z': '';
    my $opcion_crear = (!-e $self->{rutaArchivo} || $self->{forzar_creacion_archivo})? 'c' : 'r';

    my @cmd = (

        $self->__tarCmd,

        $opcion_comprimir.$opcion_crear.'pf', $self->{rutaArchivo},

        '--ignore-failed-read',

        '--exclude-caches',

    );

    push @cmd, @{$self->{tar_extra_options}} if($self->{tar_extra_options});

    push @cmd, ('-g', $self->{rutaSnap}) if($self->{rutaSnap} && !$self->{siempre_completo});

    push @cmd, @{$self->{lineaTar}};


    return join (' ', @cmd) if($self->en_mockup);

    my $runner = EixoUtils::Runner->new(die_on_error => undef);

    $runner->run(@cmd);

    unless($runner->status  == 0 || $runner->status == 1){
        die("Error. ArchivoTar->crearArchivoTar: se ha producido un error en la ejecucion del comando tar:(".
            join(' ', @cmd)."). status: ".$runner->status. '.Detalles: '.$runner->stdout.', '.$runner->stderr);
    }

    #system(@cmd);
    #my $status = $?>>8;

    #unless($status  == 0 || $status == 1){
    #    die("Error. ArchivoTar->crearArchivoTar: se ha producido un error en la ejecucion del comando tar:".join(',', @cmd).". status: $status");
    #}

    chmod($self->{permisosArchivo}, $self->{rutaArchivo}) > 0 or die("Error. ArchivoTar->crearArchivoTar : no se han podido cambiar los permisos del archivo tar. Detalles: $@");

    return 1;  
}

sub establecerPrioridad{

    my $self = $_[0];
    my $prioridad = $_[1];

    $self->{prioridad} = $prioridad; 

    $self;

}


sub desentarrar {
    my ($self, $ruta_desentarrado) = @_;

    die("Error, ArchivoTar->desentarrar: falta la ruta de desentarrado") 
        unless($ruta_desentarrado);

    unless($self->rutaArchivo){
        die("Error. ArchivoTar->desentarrar : falta la ruta del archivo tar a desentarrar");
    }

    if(!-e $RUTA_BINARIO_TAR){

        die("Error. ArchivoTar->desentarrar: no se encuentra el binario del programa tar");

    }

    push @{$self->lineaTar},(
        '--extract',
        
        '--file',

        $self->rutaArchivo,

        '--directory',

        $ruta_desentarrado,
    );

    my @cmd = (
        
        $self->__tarCmd, 

        @{$self->lineaTar}
    );

    my $runner = EixoUtils::Runner->new(die_on_error => undef);

    $runner->run(@cmd);

    unless($runner->status  == 0 ){
        die("Error. ArchivoTar->desentarrar: se ha producido un error en la ejecucion del comando tar:(".
            join(' ', @cmd)."). status: ".$runner->status. '.Detalles: '.$runner->stdout.', '.$runner->stderr);
    }

}


sub __tarCmd{

    (
        $RUTA_BINARIO_NICE,'-n',$_[0]->{prioridad},

        $RUTA_BINARIO_IONICE, '-c','3',

        $RUTA_BINARIO_TAR,
    )
}
        


#package Main;

#my $a = ArchivoTar->new;
#$a->agregarRuta('/tmp/a');
#$a->agregarRuta('/tmp/b');
#$a->establecerRutaArchivo('/tmp/tar.tar');
#$a->crearArchivoTar;
