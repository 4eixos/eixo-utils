package EixoUtils::Mongodump;

use strict;
use Eixo::Base::Clase qw(EixoUtils::Base);
use EixoUtils::Runner;
use IPC::Cmd qw(can_run);


has(
    host              => '127.0.0.1',
    port              => 27017,
    username          => undef,
    password          => undef,
    db                => undef,
    gzip              => undef,
    quiet             => 1,
    ssl               => undef,
    repair            => undef,
    excludeCollection => undef,
    collection        => undef,
    query             => undef,
    queryFile         => undef,
    archive           => undef,
    out               => undef,
    extra             => undef,

    mongodump_path => can_run('mongodump'),
);


sub initialize{
    my ($self, %args) = @_;

    $self->SUPER::initialize(%args);
    
    $self->verifyRequirements();

}

sub verifyRequirements{
    $_[0]->__checkMongodump();
}

sub __checkMongodump{
    die('mongodump not found') 
        unless(-x $_[0]->mongodump_path);
}

sub dump{
    my ($self, %args) = @_;

    my %merged_args = (%$self, %args);

    $self->mustExistParameters(
        [qw(
            host 
            db 
            archive|out)],
        \%merged_args
    );

    $self->__execMongodump(
        args => [],
        options => \%merged_args 
    );

}


sub __execMongodump{

    my ($self,%params) = @_;

    $self->__exec(
        cmd => $self->mongodump_path,
        arguments => $params{args},
        options => $params{options},


        switch_options => [qw(
            gzip
            quiet
            ssl
            repair
        )],

        general_options => [qw(
            host
            port
            username
            password
            db
            excludeCollection
            collection
            query
            queryFile
            archive
            out
        )],
    );
}

sub __processGeneralOptions{
    my ($self, %params) = @_;

    my @args = ();

    my %options = %{$params{options} || {}};

    foreach my $opt (@{$params{general_options}}){
        if(my $val = $options{$opt}){
            my $cmd_opt = $opt;
            if ( $cmd_opt eq 'excludeCollection' ) {
                my @collections = split( /\,/, $val );

                foreach my $collection (@collections) {
                    push @args, "--$cmd_opt=$collection";
                }
            }
            else {
                push @args,"--$cmd_opt=$val"; 
            }
        }
    }

    # Si hay "parametros extra", los rompemos por espacio y los incluimos en args
    if ( defined($options{extra}) && ('' ne $options{extra}) ) {
        for  ( split( /\s+/, $options{extra}) ) {
            next if $_ eq "";
            push @args, $_;
        }

    }

    return @args;
}

1;

