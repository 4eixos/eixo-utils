package EixoUtils::Validator;
use strict;

our $VERSION  = 0.1;

sub new {
    my $validations = &VALIDATIONS();

    return bless({
        validations => $validations
    })
}

sub validate {
    my ($self, $type, $value) = @_;
    
    unless(exists($self->{validations}->{$type})){
        die ("Validation to type #{type} not exists!")
    }

    while(my ($validation_class, $validation) = each(%{$self->{validations}->{$type}})){

        if($validation_class =~ /^regex/){

            return undef unless($value =~ $validation);
        }
        
        if($validation_class eq 'fixed_values'){

            return undef unless(grep {$_ eq $value} @$validation);
            
        }

        if($validation_class eq 'range'){

            my ($lower_limit, $upper_limit) = ( $validation =~ /^(\d+)\.\.(\d+)$/) ;

            $value *= 1;
            $lower_limit *= 1;
            $upper_limit *= 1;

            #print "Validando $value entre $lower_limit y $upper_limit \n";
            
            return undef unless($value >= $lower_limit && $value <= $upper_limit);
        }
    }

    1;
}

sub VALIDATIONS {
return {
  'apache_module' => {
                       'fixed_values' => [
                                           'alias',
                                           'apreq2',
                                           'auth_basic',
                                           'auth_digest',
                                           'authn_file',
                                           'authn_core',
                                           'authnz_ldap',
                                           'auth_openid',
                                           'authz_default',
                                           'authz_groupfile',
                                           'authz_host',
                                           'authz_user',
                                           'autoindex',
                                           'cgi',
                                           'dav_fs',
                                           'dav',
                                           'dav_svn',
                                           'deflate',
                                           'dir',
                                           'env',
                                           'expires',
                                           'fcgid',
                                           'headers',
                                           'ldap',
                                           'log_config',
                                           'logio',
                                           'mime',
                                           'negotiation',
                                           'perl',
                                           'php5',
                                           'proxy_ajp',
                                           'proxy_balancer',
                                           'proxy_connect',
                                           'proxy_http',
                                           'proxy',
                                           'python',
                                           'rewrite',
                                           'setenvif',
                                           'ssl',
                                           'status',
                                           'wsgi',
                                           'xsendfile',
                                           'access_compat'
                                         ]
                     },
  'revision' => {
                  'regexp' => qr/(?^:\A[\w.\-_]+\z)/
                },
  'perl_module' => {
                     'regexp' => qr/(?^:^\w+(\:\:\w+)*(\@[0-9.-_]+)?$)/
                   },
  'mysql_dbname' => {
                      'regexp' => qr/(?^i:\A[a-z0-9_]{2,63}\z)/
                    },
  'checksum' => {
                  'regexp' => qr/(?^i:^[a-z0-9]{16,64}$)/
                },
  'python_package' => {
                        'regexp' => qr/(?^:^[\w\-\.#]+$)/
                      },
  'iso639-1' => {
                  'regexp' => qr/(?^i:^[a-z]{2}$)/
                },
  'smtp_user' => {
                   'regexp' => qr/(?^x:
            ^(
                ([\w+\-.]+@[a-z\d\-.]+\.[a-z]+) | # email
                ([\w\-\_]+)                       # username
            )$)/
                 },
  'password' => {
                  'regexp' => qr/(?^:^.{6,64}$)/
                },
  'server_name' => {
                     'regexp' => qr/(?^:^(localhost|([a-z0-9-]+|\*)(\.([a-z0-9-]+|\*))*(\.([a-z]{2,}|\*)))$)/
                   },
  'postgresql_identifier' => {
                               'regexp' => qr/(?^i:\A[a-z0-9_]{2,63}\z)/
                             },
  'int' => {
             'regexp' => qr/(?^:^\d+$)/
           },
  'mysql_dbpassword' => {
                          'regexp' => qr/(?^i:\A.{6,64}\z)/
                        },
  'unix_command' => {
                      'regexp' => qr/(?^i:\A[\w\s.\-_+><:]+\z)/
                    },
  'word' => {
              'regexp' => qr/(?^i:\A[\w\-]+\z)/
            },
  'socket_address' => {
                        'regexp' => qr/(?^:^(unix|tcp|udp)?(\:\/\/)?[\/\w\.\d\:_-]+?\:?\d{0,5}$)/
                      },
  'postgresql_extra_privileges' => {
                                     'regexp' => qr/(?^x:^
            (
                SUPERUSER | NOSUPERUSER |
                CREATEDB | NOCREATEDB |
                CREATEROLE | NOCREATEROLE |
                CREATEUSER | NOCREATEUSER |
                INHERIT | NOINHERIT |
                LOGIN | NOLOGIN |
                CONNECTION LIMIT \d{1,4} |
                PASSWORD .{6,64} |
                VALID UNTIL \d{10,16}

 \           )$)/
                                   },
  'node_module' => {
                     'regexp' => qr/(?^:^[\w\.-]+$)/
                   },
  'ssh_public_key' => {
                        'regexp' => qr/(?^i:^[@%\s\w\/\+\-\.]+$)/
                      },
  'text' => {
              'regexp' => qr/(?^i:^[@%\s\w\-]+$)/
            },
  'mysql_dbuser' => {
                      'regexp' => qr/(?^i:\A[a-z0-9_]{2,16}\z)/
                    },
  'unix_path' => {
                   'regexp' => qr/(?^i:^[\w\s.\/\-_+%]+$)/
                 },
  'tcp_port' => {
                  'range' => '1..65535'
                },
  'db_password' => {
                     'regexp' => qr/(?^i:^.{6,64}$)/
                   },
  'signed_int' => {
                    'regexp' => qr/(?^:^[-+]?\d+$)/
                  },
  'email' => {
               'regexp' => qr/(?^i:\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z)/
             },
  'float' => {
               'regexp' => qr/(?^i:\A[+-]?\d+\.?\d+(e[+-]?\d+)?\z)/
             },
  'multiline_text' => {
                        'regexp' => qr/(?^i:^[\w\-\_\@\s\.\,]+)/
                      },
  'command_line_option' => {
                             'regexp' => qr/(?^:^[\w\-\:\.\/]*$)/
                           },
  'url' => {
             'regexp' => qr/(?^:^(http(s)?|ftp|ssh):\/\/(([\w\-\:]+)\@)?(([A-Za-z0-9-]+\.)*([A-Za-z0-9-]+\.[A-Za-z0-9]+))+((\/?)(([A-Za-z0-9\._\-]+)(\/){0,1}[A-Za-z0-9.-\/]*)){0,1})/
           },
  'git_bundle' => {
                    'regexp' => qr/(?^x:^
            [\w\s.\/\-_+%]+ # unix_path
            \|
 \           ([a-z\-_]+\:\/\/)?[a-z\d\-.@]+\.[a-z\d\-.:]+\/[^\|]+  # url
            \|
            [\w.\-_]+  # revision
            (\|[\w\s.\/\-_+%,]+)? # file list (optional)
            $)/
                  },
  'version' => {
                 'regexp' => qr/(?^:\A\w+\.\w+(\.\w+)?(\.\w+)?\z)/
               },
  'username' => {
                  'regexp' => qr/(?^i:\A[\w\-]+\z)/
                },
  'domain' => {
                'regexp' => qr/(?^:^(default|[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,}))$)/
              },
  'ipv4' => {
              'regexp' => qr/(?^:\A\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\z)/
            },
  'package_name' => {
                      'regexp' => qr/(?^:^[a-z0-9+-.=]+$)/
                    },
  'ruby_gem' => {
                  'regexp' => qr/(?^:^[\w\-\.#]+$)/
                },
  'host' => {
              'regexp' => qr/(?^i:\A(localhost|[a-z\d\-.]+\.[a-z]+)\z)/
            }
}
}