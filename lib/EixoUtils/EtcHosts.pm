package EixoUtils::EtcHosts;
use strict;

use Eixo::Base::Clase;

my $FILE = '/etc/hosts';


has(
    entries => [],
    file => undef,
);


sub initialize {

    my ($self, %args) = @_;

    $args{file} ||= $FILE;

    $self->SUPER::initialize(%args);

    $_[0]->__load();
}



sub addEntry {
    my ($self,$ip, @hosts) = @_;

    push @{$self->{entries}}, EixoUtils::EtcHost::entry->new( ip => $ip, hosts => [@hosts]);

    $self->__save();
}

sub getEntry {
    my ($self, $ip)  = @_;

    [grep {$_->ip eq $ip} @{$self->entries}]->[0]

}

sub delEntry{
    my ($self, $ip) = @_;

    $self->entries(
        [grep {$_->ip ne $ip} @{$self->entries}]
    );

    $self->__save();

}




sub addHost{
    my ($self, $ip, $host) = @_;

    if(my $entry = $self->getEntry($ip)){

        push @{$entry->hosts}, $host unless($entry->containsHost($host));

        $self->__save();
    
    }
    else{

        $self->addEntry($ip, $host);
    }
}

sub delHost{
    my ($self, $ip, $host) = @_;

    if(my $entry = $self->getEntry($ip)){

        $entry->hosts(
            [grep {$_ ne $host} @{$entry->hosts}]
        );

        $self->__save();
    }

}

sub getIpByHost{

    my ($self, $host) = @_;

    foreach my $entry (@{$self->entries}){
        
        return $entry->ip if($entry->containsHost($host));
    
    }

    undef;
}




sub __load {
    my $self = $_[0];

    open my $f, $self->file || die("Imposible abrir ".$self->file.": $!");

    while(my $linea = <$f>){
       next if($linea =~ /^\#/); 

       my ($ip, @hosts) = split /\s+/, $linea;

       $self->addEntry($ip, @hosts);
    }

}

sub __save{
    my $self = $_[0];

    open my $f, '>', $self->file || die("Imposible abrir para escribir ".$self->file.": $!");

    foreach my $entry (@{$self->entries}){

        print $f $entry->ip,"\t",join(',',@{$entry->hosts}),"\n";

    }

    close $f;
}



package EixoUtils::EtcHost::entry;
use Eixo::Base::Clase;

has(
    ip => undef,
    hosts => []
);

sub required {
    qw(ip hosts)
}

sub initialize {
    my ($self, %args) = @_;

    $self->SUPER::initialize(%args);

    foreach ($_[0]->required){
        die "EtcHost::entry->new: fail required parameter $_" if(!exists($_[0]->{$_}));
    }
}

sub containsHost {
    my ($self, $host) = @_;
    
    scalar(grep {$_ eq $host} @{$self->hosts})
}

1;
