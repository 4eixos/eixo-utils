package EixoUtils::Psqlrestore;

use strict;
use Eixo::Base::Clase qw(EixoUtils::Base);
use EixoUtils::Runner;
use IPC::Cmd qw(can_run);
use File::Temp;


has(
    host          => '127.0.0.1',
    port          => 5432,
    username      => undef,
    password      => undef,
    db            => undef,
    file          => undef,
    on_error_stop => 0,
    psql_path => can_run('psql'),
);


sub initialize{
    my ($self, %args) = @_;

    $self->SUPER::initialize(%args);
    
    $self->verifyRequirements();

}

sub verifyRequirements{
    $_[0]->__checkPsqlrestore();
}

sub __checkPsqlrestore{
    die('psql not found') 
        unless(-x $_[0]->psql_path);
}

sub restore{
    my ($self, %args) = @_;

    my %merged_args = (%$self, %args);

    $self->mustExistParameters(
        [qw(host db username password file)],
        \%merged_args
    );

    $self->__execPsqlrestore(
        args => [],
        options => \%merged_args 
    );

    return $self;
}

sub getPsqlOutput {
    local $/ = undef;

    $_[0]->{tempfile}->seek( 0, 0 );

    return readline($_[0]->{tempfile});
}


sub __execPsqlrestore{

    my ($self,%params) = @_;

    $self->{tempfile} = File::Temp->new();

    $self->__exec(
        cmd => $self->psql_path,
        arguments => $params{args},
        options => $params{options},


        general_options => [qw(
            host
            port
            username
            password
            db
            file
            on_error_stop
        )],
    );
}

sub __processGeneralOptions{
    my ($self, %params) = @_;

    my @args = ();

    # psql -h pgsql02.dinaserver.com -U victorusuario -p 5432 -d victortest -f /etc/passwd

    my %options = %{$params{options} || {}};

    push(@args, '-h', $options{host}, '-p', $options{port}, '-U', $options{username},
         '-d', $options{db}, '-v', 'ON_ERROR_STOP='.$options{on_error_stop}, '-q',
         '-P', 'pager=off' ,'-f', $options{file}, '-o', $self->{tempfile}->filename());

    return @args;
    

}

sub prepareEnv{
    my($self, %params) = @_;

    my $env = {
        PGPASSWORD => $params{options}->{password},
    };

    return $env;
    
}

1;

