package EixoUtils::AzCopy;
use strict;
use Eixo::Base::Clase qw(EixoUtils::Base);
use EixoUtils::Runner;
use IPC::Cmd qw(can_run);


has(
    source                      => undef,
    source_key                  => undef,
    destination                 => undef,
    dest_key                    => undef,

    recursive                   => undef,
    exclude_older               => undef,
    exclude_newer               => undef,
    preserve_last_modified_time => undef,

    quiet => 1,

    azcopy_path => can_run('azcopy'),
);


sub initialize{
    my ($self, %args) = @_;

    $self->SUPER::initialize(%args);
    
    $self->verifyRequirements();

}

sub verifyRequirements{
    $_[0]->__checkAzcopy();
}

sub __checkAzcopy{
    die('azcopy not found') 
        unless(-x $_[0]->azcopy_path);
}


sub download{
    my ($self, %args) = @_;

    my %merged_args = (%$self, %args);

    $self->mustExistParameters(
        [qw(source destination source_key)],
        \%merged_args
    );

    $self->shoudNotExistParameters(
        [qw(dest_key)],
        \%merged_args
    );

    $self->__execAzcopy(
        options => \%merged_args
    );

}

sub upload{
    my ($self, %args) = @_;

    my %merged_args = (%$self, %args);

    $self->mustExistParameters(
        [qw(source destination dest_key)],
        \%merged_args
    );

    $self->shoudNotExistParameters(
        [qw(source_key)],
        \%merged_args
    );

    $self->__execAzcopy(
        options => \%merged_args
    );

}


sub copy{
    my ($self, %args) = @_;

    my %merged_args = (%$self, %args);

    $self->mustExistParameters(
        [qw(source destination source_key dest_key)],
        \%merged_args
    );

    $self->__execAzcopy(
        options => \%merged_args
    );

}


sub __execAzcopy{

    my ($self,%params) = @_;

    $self->__exec(
        cmd => $self->azcopy_path,
        arguments => $params{args},
        options => $params{options},


        switch_options => [qw(

            recursive
            exclude_newer
            exclude_older
            preserve_last_modified_time
            quiet
        )],

        general_options => [qw(
            source
            destination
            source_key
            dest_key
        )],
    );

}

1;
