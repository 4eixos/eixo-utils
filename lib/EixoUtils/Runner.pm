package EixoUtils::Runner;

use strict;
use Eixo::Base::Clase;

use POSIX ":sys_wait_h";
use IPC::Open3;
use Symbol;
use Fcntl qw(SEEK_SET SEEK_CUR SEEK_END);
use IO::Select;
use Carp;

has(
    command =>  [],
    die_on_error =>  undef,
    in_shell => undef,
   
    status => undef,
    stdout => '',
    stderr => '',
    env => {},
    en_mockup => undef,

    timeout => undef,

    on_change=> sub {},
);


sub run {&launch(@_)}

sub launch{

    my ($self, @comando) = @_;

    if(scalar(@comando) == 1){
        @comando = $self->__trocearComando($comando[0]);
    }

    @comando = @{$self->{command}} unless(@comando);

    $self->{command} = \@comando;
    $self->{status} = undef;
    $self->{stdout} = '';
    $self->{stderr} = '';

    use IO::File;

   # my ($MYOUT) = IO::File->new_tmpfile;

   # my ($MYERR) = IO::File->new_tmpfile;

    my ($MYOUT, $MYERR) = (gensym, gensym);

    # set environment
    local %ENV = (%ENV, %{$self->{env}});

    # 
    # si estamos en mockup vamos a ver el comando en vez de ejecutarlo
    #
    if($self->en_mockup){
        unshift @comando, 'echo';
    }

    my $pid = open3(
            
            my $stdin = gensym(),

            $MYOUT,
            $MYERR,
            
            ($self->in_shell)? join (' ', @comando):  @comando
    );


    $self->__esperarEjecucion($pid, $MYOUT, $MYERR);

    #seek($_,0,SEEK_SET) for $MYOUT, $MYERR;

    #my ($salida, $salida_error);

    #{

    #    local $/ = undef;        
    #    $salida = <$MYOUT>;       
    #    $salida_error = <$MYERR>; 
    #}

    #$self->{stdout} = $salida;

    #chomp($self->{stdout}) if($salida);

    ##$salida_error =~ s/\n+//g;

    #$self->{stderr} = $salida_error;

    if($? == -1){

        $self->{status} = -1;

        croak('Error al ejecutar '.join(' ' , @comando));

    }
    elsif($? & 127){

        croak('Comando '. join(' ',@comando) . ' finalizado inexperadamente con SIGNAL = '.($? & 127));
    }                                                                                                    
    else{

        my $estado = $?>>8;

        $self->{status} = $estado;

        if($estado != 0){
            my $stdout = $self->stdout;
            my $stderr = $self->stderr;

            $stdout =~ s/\n/ /g;
            $stderr =~ s/\n/ /g;
       
            croak('Error al ejecutar ['.join (' ', @{$self->command})."]\nSTATUS: ".$self->status."\nSTDOUT: ".$stdout."\nSTDERR: ".$stderr) 
                if($self->{die_on_error});         
        }
                                                                                                          
    }

    return $self;
}

sub __esperarEjecucion{
    my ($self, $pid, $h_out, $h_err) = @_;

    if(my $f_update = $self->on_change){

        #$h_err->autoflush(1);
        #$h_out->autoflush(1);
        $h_out->blocking(0);
        $h_err->blocking(0);

        my $select = IO::Select->new;            

        $select->add($h_out);
        $select->add($h_err);

        seek($h_out, 0, SEEK_SET);
        seek($h_err, 0, SEEK_SET);

        my $salir = undef;

        while(!$salir){

            my @ready = $select->can_read(0.1);

            foreach my $f (@ready){


                my $texto = $self->leer($f);

                $f_update->($texto) if($texto);
                
                if($f == $h_out){
                    $self->{stdout} .= $texto;
                }
                else{
                    $self->{stderr} .= $texto;
                }

                #seek($f, 0, SEEK_CUR);

            }

            $salir = 1 if(waitpid($pid, WNOHANG));
        }
    }
    else{
        my $salir = undef;

        my $timeout = ($self->timeout())? $self->timeout() : undef;

        do{
            $salir = 1 if(waitpid($pid, WNOHANG));

            #print "t=$timeout\n";

            sleep(1);

            if(defined($timeout)) {
            
                $timeout--;

                croak("TIMEOUT (".$self->timeout."s) alcanzado ejecutando '".$self->command()->[0]."'") if($timeout <= 0);
            }

        }while(!$salir);

        # 
        # recoger salida
        #
        {

            local $/ = undef;        
            $self->{stdout} = <$h_out>;
            $self->{stderr} = <$h_err>;
        }
    }
    
    chomp($self->{stdout});
    
}

sub leer{
    my ($self, $fh) = @_;

    my $buffer = "";
    my $max = 2 * (400);

    while(1){

        my $m = "";

        my $leidos = sysread($fh, $m, 400);

        $buffer .= $m if($m);

        $max -= $leidos if($leidos);

        last if(!$leidos || $max <= 0);
    }
    
    return $buffer;
}

sub __trocearComando{
    my ($self, $cmd) = @_;

    my @cmd;
    # 
    # regexp que permite partir por espacios y por bloques entrecomillados
    #
    while($cmd =~ /((\"[^"]+\")|(\'[^']+\')|([^\s]+))\s?/g){
        my $token = $1; 
        $token =~ s/(^["']|["']$)//g;
        push @cmd, $token;
    }

    @cmd;
}


sub status_ok {

    ($_[0]->status == 0)

}

1;

