package EixoUtils::PuntoMontaje;

use strict;

my %has =  (
    dispositivo => '',
    punto_montaje => '',
    sistema_archivos => '',
    opciones => '',
    dump_freq => '',
    pass_num => '',
);

sub tiene { return %has }

sub new{
    my ($class, %args) = @_;

    my $self = {};

    while(my ($k, $v) = each(%has)){

        $self->{$k} = (exists($args{$k}))? $args{$k} : $v;

    }

    $self;
}


1;
