package EixoUtils::Fstab;
use strict;
use EixoUtils::PuntoMontaje;
use File::Copy;

my $FSTAB_FILE = '/etc/fstab';

sub new{
    bless({filesystems => []});
}

sub cargarFstab{
    
    my $self = $_[0];
    
    open my $f, $FSTAB_FILE;
    
    while(my $linea = <$f>) {

        next if ($linea =~ /^#|^$/);
        
        my ($dispositivo, 
        $punto_montaje, 
        $sistema_archivos, 
        $options, 
        $dump_freq, 
        $pass_num) = split /\s+/, $linea;
        
        push @{$self->{filesystems}}, 
        
            EixoUtils::PuntoMontaje->new(
                dispositivo => $dispositivo,
                punto_montaje => $punto_montaje,
                sistema_archivos => $sistema_archivos,
                opciones => $options,
                dump_freq => $dump_freq,
                pass_num => $pass_num
            );
    }

    $self;
}

sub addDispositivo{
    my ($self, %args) = @_;
    
    return if( grep {

            $args{dispositivo} eq $_->{dispositivo} &&
            $args{punto_montaje} eq $_->{punto_montaje}
        
        } @{$self->{filesystems}});

    my $p = PuntoMontaje->new(%args);
    
    push @{$self->{filesystems}}, $p;
    
    $self;

}

sub save{
    my ($self) = @_;

    $self->backup();
    
    open my $f, '>', $FSTAB_FILE;
    
    foreach my $p (@{$self->{filesystems}}){
    
    	print $f $p->{dispositivo}. "\t". 
                     $p->{punto_montaje}."\t".
                     $p->{sistema_archivos}."\t".
                     $p->{opciones}."\t".
                     $p->{dump_freq}."\t".
                     $p->{pass_num}."\n";
    
    }
    
    close $f;
}


sub backup {

    copy($FSTAB_FILE, $FSTAB_FILE.'.old')

}


sub get {
    my ($self, %args) = @_;

    grep {&filtros($_, %args)} @{$self->{filesystems}}

}

sub filtros{
    my ($p,%args) = @_;

    my $res = 1;

    my %atributos = PuntoMontaje->tiene;

    foreach my $attr (keys(%atributos)){


        if(exists($args{$attr})){
            #print "comparando ".$args{$attr}." contra el att $attr".$p->$attr."\n";

            if(ref $args{$attr} eq 'Regexp'){
                $res = ($p->$attr =~ /$args{$attr}/);
            }
            else{
                $res = ($p->$attr eq $args{$attr});
            }

            return undef unless($res);
        }

    }

    $res;
}

1;
