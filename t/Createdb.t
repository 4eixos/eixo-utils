use t::test_base;
use IO::Handle;
use IPC::Cmd qw(can_run);
use_ok("EixoUtils::Createdb");

SKIP: {
    skip "createdb NO INSTALADO", 2 unless(can_run('createdb'));

    EixoUtils::Createdb->debugOn();

    my ( $host, $username, $password, $db ) = (
        'psql.example.com',
        'username1',
        'p455w0rd1',
        'mydatabase',
    );

    my $createdb = EixoUtils::Createdb->new(
        host     => $host,
        username => $username,
        password => $password,
        db       => $db,
        # Solo hacemos echo del comando
        dry_run  => 1,
    )->create();

    ok(
        $createdb->output =~ /\/usr\/bin\/createdb/ &&
        $createdb->output =~ /\s+$db/ &&

        "Comando para crear base de datos, generado correctamente: ".$createdb->output
    );

};


done_testing();

1;
