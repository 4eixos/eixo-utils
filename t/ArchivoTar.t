use t::test_base;
use File::Temp qw(tempdir);
use IO::Handle;

use_ok("EixoUtils::ArchivoTar");
use_ok("EixoUtils::Runner");

my $tmpdir = tempdir( CLEANUP => 1 );

my $l = "/usr/bin/nice -n 19 /usr/bin/ionice -c 3 /bin/tar zcpf /tmp/MgrPYxJoTS/file.tar.gz --ignore-failed-read --exclude-caches -C t/ data/";

my $tar = EixoUtils::ArchivoTar->new(en_mockup => 1)
    ->agregarRuta('data/')
    ->rutaArchivo($tmpdir."/file.tar.gz")
    ->tar_extra_options(['-C', 't/']);

    
my $tar_line = $tar->crearArchivoTar;

ok(
    $tar_line =~ qr!^/usr/bin/nice -n 19 /usr/bin/ionice -c 3 /bin/tar zcpf /tmp/\w+/file.tar.gz! &&
    $tar_line =~ qr!\-C t/ data\/$! ,

    "Ejecuta el comando correcto"
);

$tar->en_mockup(0)->crearArchivoTar;

ok(
    -f $tmpdir."/file.tar.gz",
    "Fichero tar creado correctamente"
);


eval{
    EixoUtils::ArchivoTar->new(
        rutaArchivo => "$tmpdir/file.tar.gz",
     )->desentarrar($tmpdir)
};

ok(
    !$@,
    "Extraccion de fichero tar correcta"
);


#my $runner = EixoUtils::Runner->new
#    ->run("tar xf $tmpdir/file.tar.gz -C $tmpdir");

#ok(
#
#    $runner->status == 0,
#    "Fichero tar bien construido"
#);

ok(
    -f $tmpdir."/data/file1" &&
    -f $tmpdir."/data/file2",

    "El tar contiene los archivos solicitados"
);
    
done_testing;

