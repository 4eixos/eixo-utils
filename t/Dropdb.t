use t::test_base;
use IO::Handle;
use IPC::Cmd qw(can_run);
use_ok("EixoUtils::Dropdb");

SKIP: {
    skip "dropdb NO INSTALADO", 2 unless(can_run('dropdb'));

    EixoUtils::Dropdb->debugOn();

    my ( $host, $username, $password, $db ) = (
        'psql.example.com',
        'username1',
        'p455w0rd1',
        'mydatabase',
    );

    my $dropdb = EixoUtils::Dropdb->new(
        host     => $host,
        username => $username,
        password => $password,
        db       => $db,
        # Solo hacemos echo del comando
        dry_run  => 1,
    )->drop();

    ok(
        $dropdb->output =~ /\/usr\/bin\/dropdb/ &&
        $dropdb->output =~ /\s+$db/ &&

        "Comando para borrar base de datos, generado correctamente: ".$dropdb->output
    );

};


done_testing();

1;
