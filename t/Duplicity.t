use t::test_base;
use File::Temp qw(tempdir);
use File::Path qw(make_path remove_tree);
use Archive::Tar;
use IO::Handle;
use IPC::Cmd qw(can_run);
use_ok("EixoUtils::Duplicity");

SKIP: {

    skip "DUPLICITY NO INSTALADO", 2 unless(can_run('duplicity'));

    
    my $tmpdir = tempdir( CLEANUP => 0 );
    my $archive_dir = tempdir( CLEANUP => 0 );
    
    my ($source_path, $target_url, $archive_dir) = (
        "t/data/",
        "file://$tmpdir",
        #"t/duplicity_archive_dir"
        $archive_dir
    );
    
    # 
    # archive-dir gestionado de forma manual
    #
    #ok(
    #    remove_tree($archive_dir, {keep_root => 1}),
    #    "Limpiamos el archive-dir"
    #);
    
    #EixoUtils::Duplicity->debugOn();

    my $dup = EixoUtils::Duplicity->new(
        source_path => $source_path,
        target_url => $target_url,
        archive_dir => $archive_dir,
        no_encryption => 1,
    );
    
    # 
    # chequear a inicialización de atributos
    #
    ok(
        $dup->source_path eq $source_path &&
        $dup->target_url eq $target_url &&
        $dup->archive_dir eq $archive_dir,
    
        'Objeto duplicity instanciado correctamente'
    );
    
    # 
    # chequear a realizacion de backup sin encriptacion
    #
    $dup->backup(
        include => ['t/data/file1'],
        exclude => ['**/file2'],
    );
    
    my @manifest = cargarFilesDir($tmpdir,'\.manifest');
    my @difftar = cargarFilesDir($tmpdir,'\.difftar\.?');
    my @sigtar = cargarFilesDir($tmpdir,'\.sigtar\.?');
    
    ok(
        @manifest > 0 && @difftar > 0 && @sigtar > 0,
        "Se han generado los ficheros de backup"
    );
    
    my $fecha_backup_inicial = (split(/\./,$manifest[0]))[1];
    print "FECHA BACKUP INICIAL: $fecha_backup_inicial\n";
    
    my $tar = Archive::Tar->new($difftar[0]);
    
    ok(
        $tar->contains_file("snapshot/file1"),
    
        "El tar se ha generado correctametne"
    );
    
    
    ## backup incremental
    open my $f, '>', "t/data/test"; close $f;
    
    $dup->backup();
    
    my @manifest = cargarFilesDir($tmpdir,'\.manifest');
    my @difftar = cargarFilesDir($tmpdir,'\.difftar\.?');
    my @sigtar = cargarFilesDir($tmpdir,'\.sigtar\.?');
    
    ok(
        @manifest == 2 && @difftar == 2 && @sigtar == 2,
        "Se han generado los ficheros de backup"
    );
    
    ## verify
    # borramos un elemento del origen y chequeamos que el backup falla al verificarse
    unlink("t/data/test");
    
    $dup->verify(
        source_url => $target_url, 
        target_folder => $source_path,
        compare_data => 1
    );
    
    ok(
        $dup->status == 1 &&
        $dup->error =~ /error in command \(.+duplicity verify/i,
        
        "La verificacion falla porque el origen ha cambiado"
    );
    
    
    ## listfiles del ultimo backup
    my $listado = $dup->listCurrentFiles(
        source_url => $target_url, 
    )->output;
    
    ok(
        $listado =~ /file2/m && $listado =~ /test/m,
    
        "Se listan los ficheros del ultimo backup"
    );
    
    $listado = $dup->listCurrentFiles(
        source_url => $target_url, 
        time => $fecha_backup_inicial
    )->output;
    
    ok(
        $listado =~ /file1/m && $listado !~ /test/m,
    
        "En el primer backup no existía el fichero de test"
    );
    
    ## chequear estado da coleccion de backups
    my $status = $dup->collectionStatus(
        source_url => $target_url
    )->output;
    
    ok(
        $status =~ /chain-complete\n\sfull\s.+\n\sinc\s.+/m,
        
        "El status de la coleccion de backups es el esperado"
    );
    
    ## restaurar un backup
    my $tmpdir_restore = tempdir( CLEANUP => 1 );
    print $tmpdir_restore, "\n";
    $dup->restore(
        source_url => $target_url,
        restoration_path => $tmpdir_restore
    );
    ok(
        -e "$tmpdir_restore/file1" &&
        -e "$tmpdir_restore/test",
        
        "Restaurando el ultimo backup y chequeamos contenido"
    );
    
    my $tmpdir_restore2 = tempdir( CLEANUP => 1 );
    print $tmpdir_restore2, "\n";
    $dup->restore(
        source_url => $target_url, 
        time => $fecha_backup_inicial,
        restoration_path => $tmpdir_restore2
    );
    ok(
        -e "$tmpdir_restore2/file1" &&
        !-e "$tmpdir_restore2/test",
    
        "Restaurando contenido backup inicial"
    );

};

## borrar mas antiguos
#$dup->removeOlderThan()

# chequear a realización de backup con encryptacion simetrica


# chequear a realizacion de backup con encriptacion asimétrica, especificando un secret_keyring



done_testing();
