use strict;
use warnings;

use Test::More;
use Data::Dumper;
use Carp;


sub cargarFilesDir{
    my ($dir, $pattern) = @_;

    return grep {/$pattern/} glob("$dir/*");
    
}


1;

