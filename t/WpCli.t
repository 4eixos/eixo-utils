use t::test_base;

use_ok("EixoUtils::WpCli");
use IPC::Cmd qw(can_run);
use Data::Dumper;

SKIP: {

    skip "wp-cli NOT INSTALLED", 2 unless(can_run('wp'));

    EixoUtils::WpCli->debugOn();

    my $wp = EixoUtils::WpCli->new(
    
        dry_run => 1
    
    )->core(
        qw(download --a=b), 
        {debug =>1, path => '/a'}
    );

    ok(
        $wp->output =~ m|wp core download --a=b --path=/a --allow-root --debug|,

        "Command db export generated correctly: ".$wp->output
    );


};

done_testing();
