use strict;
use t::test_base;
use Cwd 'abs_path';
use File::Temp;
use File::Basename;


use_ok("EixoUtils::Zip");

my $zip_file = dirname(abs_path($0)).'/test.zip';

my $tempdir = File::Temp->newdir;

ok(
    EixoUtils::Zip::unzip($zip_file, $tempdir),
    'run EixoUtils::Zip::unzip'
);

ok(
    (
        -f "$tempdir/file1" &&
        -d "$tempdir/dir1" &&
        -f "$tempdir/dir1/file2" && 
        -s "$tempdir/file1" == 1024 &&
        -s "$tempdir/dir1/file2" == 2048
    ),

    'check extracted files'
);

done_testing;

