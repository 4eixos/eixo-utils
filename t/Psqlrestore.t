use t::test_base;
use File::Temp qw(tempdir);
use File::Path qw(make_path remove_tree);
use IO::Handle;
use IPC::Cmd qw(can_run);
use_ok("EixoUtils::Psqlrestore");

SKIP: {

    skip "psql NO INSTALADO", 2 unless(can_run('psql'));

    
    my $tmpdir = tempdir( CLEANUP => 0 );
    my $archive_dir = tempdir( CLEANUP => 0 );
    
    EixoUtils::Psqlrestore->debugOn();

    my ( $host, $username, $password, $db, $excludeCollection, $archive ) = (
        'pgsql.example.com',
        'user1',
        'p455w0rd1',
        'mydatabase',
        'collection1',
        '/tmp/backup_'.time().'.sql',
    );

    my $pgdump = EixoUtils::Psqlrestore->new(
        host     => $host,
        username => $username,
        password => $password,
        db       => $db,
        file     => $archive,
    
        dry_run => 1
    
    )->restore();

    ok(
        # /usr/bin/psql -h pgsql.example.com -p 5432 -U user1 -d mydatabase -f /tmp/backup_1521136806.sql
        $pgdump->output =~ /-h\s$host/ &&
        $pgdump->output =~ /-d\s$db/ &&
        $pgdump->output =~ /-U\s$username/ &&
        $pgdump->output =~ /-f\s$file/ &&

        "Comando para backup de base de datos, generado correctamente: ".$pgdump->output
    );
};

done_testing();
    

