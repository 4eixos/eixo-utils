use t::test_base;
use File::Temp qw(tempdir);
use File::Path qw(make_path remove_tree);
use IO::Handle;
use IPC::Cmd qw(can_run);
use_ok("EixoUtils::Sed");

SKIP: {

    skip "sed NO INSTALADO", 2 unless(can_run('sed'));

    EixoUtils::Sed->debugOn();

    my ( $file, $target, $replace ) = (
        '/tmp/file',
        'string1',
        'string2'
    );

    my $match = "s/$target/$replace/";

    my $sed = EixoUtils::Sed->new(
        file    => $file,
        oldtext => $target,
        newtext => $replace,
    
        dry_run => 1
    
    )->replace();

    ok(
        $sed->output =~ /\Q\/bin\/sed\E/ &&
        $sed->output =~ /\Q$match\E/ &&
        $sed->output =~ /\Q$file\E/ &&

        "Comando para reemplazar cadena generado correctamente: " . $sed->output
    );
};

done_testing();
    

