use t::test_base;
use File::Temp qw(tempdir);
use File::Path qw(make_path remove_tree);
use IO::Handle;
use IPC::Cmd qw(can_run);
use_ok("EixoUtils::Mongo");

SKIP: {

    skip "mongo NO INSTALADO", 2 unless(can_run('mongo'));

    
    my $tmpdir = tempdir( CLEANUP => 0 );
    my $archive_dir = tempdir( CLEANUP => 0 );
    
    EixoUtils::Mongo->debugOn();

    my ( $host, $username, $password, $db, $archive ) = (
        'mongodb.example.com',
        'user1',
        'p455w0rd1',
        'mydatabase',
        '/tmp/backup_'.time(),
    );

    my $mongo = EixoUtils::Mongo->new(
        host              => $host,
        username          => $username,
        password          => $password,
        db                => $db,
        script => $archive,
    
        dry_run => 1
    
    )->restore();

    ok(
        $mongo->output =~ /\Q--host=$host\E/ &&
        $mongo->output =~ /\Q--username=$username\E/ &&
        $mongo->output =~ /\Q--password=$password\E/ &&
        $mongo->output =~ /\s+\Q$db\E/ &&
        $mongo->output =~ /\s+\Q$archive\E/ &&

        "Comando para mongo, generado correctamente: ".$mongo->output
    );
};

done_testing();
    

