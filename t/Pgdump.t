use t::test_base;
use File::Temp qw(tempdir);
use File::Path qw(make_path remove_tree);
use IO::Handle;
use IPC::Cmd qw(can_run);
use_ok("EixoUtils::Pgdump");

SKIP: {

    skip "pgdump NO INSTALADO", 2 unless(can_run('pg_dump'));

    
    my $tmpdir = tempdir( CLEANUP => 0 );
    my $archive_dir = tempdir( CLEANUP => 0 );
    
    EixoUtils::Pgdump->debugOn();

    my ( $host, $username, $password, $db, $excludeCollection, $archive ) = (
        'pgsql.example.com',
        'user1',
        'p455w0rd1',
        'mydatabase',
        'collection1',
        '/tmp/backup_'.time(),
    );

    my $pgdump = EixoUtils::Pgdump->new(
        host     => $host,
        username => $username,
        password => $password,
        db       => $db,
        file     => $archive,
    
        dry_run => 1
    
    )->dump();

    ok(
        $pgdump->output =~ /--dbname=postgresql:\/\/$host\:\d+\/$db/ &&
        $pgdump->output =~ /\Q-U $username\E/ && $pgdump->env->{PGPASSWORD} eq $password && 
        $pgdump->output =~ /\Q--file=$archive\E/ &&
        $pgdump->output !~ /\Q--create\E/ &&
        $pgdump->output !~ /\Q--clean\E/ &&

        "Comando para backup de base de datos, generado correctamente: ".$pgdump->output
    );

    # Probamos opcion create
    $pgdump = EixoUtils::Pgdump->new(
        host     => $host,
        username => $username,
        password => $password,
        db       => $db,
        file     => $archive,
        create   => 1,
    
        dry_run => 1
    
    )->dump();

    ok(
        $pgdump->output =~ /--dbname=postgresql:\/\/$host\:\d+\/$db/ &&
        $pgdump->output =~ /\Q-U $username\E/ && $pgdump->env->{PGPASSWORD} eq $password && 
        $pgdump->output =~ /\Q--file=$archive\E/ &&
        $pgdump->output =~ /\Q--create\E/ &&
        $pgdump->output !~ /\Q--clean\E/ &&

        "Comprobamos que el comando lleva --create".$pgdump->output
    );

    # Probamos opcion clean
    $pgdump = EixoUtils::Pgdump->new(
        host     => $host,
        username => $username,
        password => $password,
        db       => $db,
        file     => $archive,
        clean=> 1,
    
        dry_run => 1
    
    )->dump();

    ok(
        $pgdump->output =~ /--dbname=postgresql:\/\/$host\:\d+\/$db/ &&
        $pgdump->output =~ /\Q-U $username\E/ && $pgdump->env->{PGPASSWORD} eq $password && 
        $pgdump->output =~ /\Q--file=$archive\E/ &&
        $pgdump->output =~ /\Q--clean\E/ &&
        $pgdump->output !~ /\Q--create\E/ &&

        "Comprobamos que el comando lleva --create".$pgdump->output
    );

    # Probamos opciones create y clean
    $pgdump = EixoUtils::Pgdump->new(
        host     => $host,
        username => $username,
        password => $password,
        db       => $db,
        file     => $archive,
        create   => 1,
        clean    => 1,
    
        dry_run => 1
    
    )->dump();

    ok(
        $pgdump->output =~ /--dbname=postgresql:\/\/$host\:\d+\/$db/ &&
        $pgdump->output =~ /\Q-U $username\E/ && $pgdump->env->{PGPASSWORD} eq $password && 
        $pgdump->output =~ /\Q--file=$archive\E/ &&
        $pgdump->output =~ /\Q--create\E/ &&
        $pgdump->output =~ /\Q--clean\E/ &&

        "Comprobamos que el comando lleva --create y --clean".$pgdump->output
    );
};

done_testing();
    

