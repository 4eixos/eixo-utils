use strict;
use t::test_base;

SKIP: {
    my $use_dbi = eval "require 'DBI.pm' && require 'DBD/mysql.pm'";

    skip "DBI not installed", 1 unless($use_dbi);

    use_ok("EixoUtils::MysqlTool");
    
}

done_testing();
