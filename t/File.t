use strict;
use t::test_base;
use File::Temp;


use_ok("EixoUtils::File");

my $tmp = File::Temp->new;
$tmp->autoflush(1);

my $test_string =<<'eof'
    Test string
    asfdojrds
    afspj32raf
eof
;

print $tmp $test_string;

ok(
    (EixoUtils::File::get_contents($tmp->filename) eq $test_string),
    
    "EixoUtils::File::get_contents from a filename"
);

ok(
    (EixoUtils::File::get_contents('file://'.$tmp->filename) eq $test_string),
    
    "EixoUtils::File::get_contents from a file://filename"
);

like(
    EixoUtils::File::get_contents('http://google.com'),
    qr/google/,
    'EixoUtils::File::get_contents from a http://google.com'
);


done_testing;
