use t::test_base;
use File::Temp qw(tempfile);
use IO::Handle;

use_ok("EixoUtils::EtcHosts");


my ($fh, $tempfile) = tempfile();

while(my $line = <DATA>){

    print $fh $line;
}

$fh->flush();

my $test_ip = '1.1.1.1';
my $test_host = 'mytesthost.example.com';
my $test_host2 = 'mytesthost2.example.com';

my $etchost = EixoUtils::EtcHosts->new(
    file => $tempfile
);

ok( 
    scalar(@{$etchost->entries}) == 5, 

    "File has correct number of entries"
);

ok(
    $etchost->addEntry($test_ip, $test_host) && $etchost->getEntry($test_ip),

    "Entry added correctly"
);

ok(
    $etchost->addHost($test_ip, $test_host2) && 
        ($etchost->getIpByHost($test_host2) eq $test_ip),

    "Host entry added correctly"
);

ok(
    $etchost->delHost($test_ip, $test_host2) && 
        !$etchost->getIpByHost($test_host2) ,

    "Host entry removed correctly"
);


ok(
    $etchost->delEntry($test_ip) && !$etchost->getEntry($test_ip),

    "Entry removed correctly"
);


done_testing();


__DATA__
127.0.0.1       localhost
127.0.1.1       sobremesa
#test comments
123.23.12.1     test.test.es
132.25.22.3     pest.test.com
172.17.0.6      rist.fsad.es test2.test.es 
