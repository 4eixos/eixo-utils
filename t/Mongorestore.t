use t::test_base;
use File::Temp qw(tempdir);
use File::Path qw(make_path remove_tree);
use IO::Handle;
use IPC::Cmd qw(can_run);
use_ok("EixoUtils::Mongorestore");

SKIP: {

    skip "mongorestore NO INSTALADO", 2 unless(can_run('mongorestore'));

    
    my $tmpdir = tempdir( CLEANUP => 0 );
    my $archive_dir = tempdir( CLEANUP => 0 );
    
    EixoUtils::Mongorestore->debugOn();

    my ( $host, $username, $password, $db, $archive ) = (
        'mongo.example.com',
        'user1',
        'p455w0rd1',
        'mydatabase',
        '/tmp/backup_'.time().'.gz',
    );

    my $mr = EixoUtils::Mongorestore->new(
        host     => $host,
        username => $username,
        password => $password,
        db       => $db,
        archive     => $archive,
    
        dry_run => 1
    
    )->restore();

    ok(
        # /usr/bin/psql -h pgsql.example.com -p 5432 -U user1 -d mydatabase -f /tmp/backup_1521136806.sql
        $mr->output =~ /\Q--host=$host\E/ &&
        $mr->output =~ /\Q--db=$db\E/ &&
        $mr->output =~ /\Q--username=$username\E/ &&
        $mr->output =~ /\Q--archive=$file\E/ &&

        "Comando para restaurar backup de bd mongo, generado correctamente: ".$mr->output
    );
};

done_testing();
