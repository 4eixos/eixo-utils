use t::test_base;
use File::Temp qw(tempdir);
use IO::Handle;

use_ok("EixoUtils::Runner");

my $cmd = "ls -l 'dir con espacios'";

my $runner = EixoUtils::Runner->new(en_mockup => 1);

$runner->run($cmd);
my @cmd = @{$runner->command};


ok(
    scalar(@cmd) == 4 &&

    $cmd[0] eq 'echo' &&
    $cmd[1] eq 'ls' &&
    $cmd[2] eq '-l' &&
    $cmd[3] eq 'dir con espacios' ,

    "Comando correctamente troceado"
);

done_testing();


