use t::test_base;
use File::Temp qw(tempdir);
use File::Path qw(make_path remove_tree);
use IO::Handle;
use IPC::Cmd qw(can_run);
use_ok("EixoUtils::AzCopy");

SKIP: {

    skip "azcopy NO INSTALADO", 2 unless(can_run('azcopy'));

    
    my $tmpdir = tempdir( CLEANUP => 0 );
    my $archive_dir = tempdir( CLEANUP => 0 );
    
    EixoUtils::AzCopy->debugOn();

    my ($source, $destination, $source_key, $destination_key) = (
        'azure://source',
        'azure://destination',
        'my_origin_key',
        'my_destination_key'
    );

    my $azcopy = EixoUtils::AzCopy->new(
    
        source => $source,
        source_key => $source_key,
        dry_run => 1
    
    )->copy(
    
        destination => $destination,
        destination_key => $destination_key,
    
    );

    ok(
        $azcopy->output =~ /\Q--source $source\E/ &&
        $azcopy->output =~ /\Q--destination $destination\E/ &&
        $azcopy->output =~ /\Q--destination-key $destination_key\E/ &&
        $azcopy->output =~ /\Q--source-key $source_key\E/ ,

        "Comando para copiar 2 volumenes remotos, generado correctamente: ".$azcopy->output
    );
};

done_testing();
    

