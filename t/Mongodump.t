use t::test_base;
use File::Temp qw(tempdir);
use File::Path qw(make_path remove_tree);
use IO::Handle;
use IPC::Cmd qw(can_run);
use_ok("EixoUtils::Mongodump");

SKIP: {

    skip "mongodump NO INSTALADO", 2 unless(can_run('mongodump'));

    
    my $tmpdir = tempdir( CLEANUP => 0 );
    my $archive_dir = tempdir( CLEANUP => 0 );
    
    EixoUtils::Mongodump->debugOn();

    my ( $host, $username, $password, $db, $excludeCollection, $archive ) = (
        'mongodb.example.com',
        'user1',
        'p455w0rd1',
        'mydatabase',
        'collection1,collection2',
        '/tmp/backup_'.time(),
    );

    my $mongodump = EixoUtils::Mongodump->new(
        host              => $host,
        username          => $username,
        password          => $password,
        db                => $db,
        excludeCollection => $excludeCollection,
        archive           => $archive,
    
        dry_run => 1
    
    )->dump();

    ok(
        $mongodump->output =~ /\Q--host=$host\E/ &&
        $mongodump->output =~ /\Q--username=$username\E/ &&
        $mongodump->output =~ /\Q--password=$password\E/ &&
        $mongodump->output =~ /\Q--db=$db\E/ &&
        $mongodump->output =~ /\Q--excludeCollection=collection1\E/ &&
        $mongodump->output =~ /\Q--excludeCollection=collection2\E/ &&
        $mongodump->output =~ /\Q--archive=$archive\E/ &&

        "Comando para backup de base de datos, generado correctamente: ".$mongodump->output
    );
};

done_testing();
    

